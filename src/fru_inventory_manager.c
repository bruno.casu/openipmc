
/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file fru_inventory_manager.c
 * 
 * @author Bruno Augusto Casu
 * 
 * @brief  Management interface for the FRU Inventory creation and access.
 */

#include "FreeRTOS.h"

#include "string.h"
#include "stdint.h"

#include "fru_inventory_manager.h"

uint8_t *fru_inventory;
size_t fru_inventory_size = 0;

/*
 * ALLOCATION AND CREATION OF ARRAY STRUCTURE FOR INVENTORY BOARD INFO
 */
size_t create_board_info_field (uint8_t board_info[],
                                uint32_t mfg_date_time,
                                char * const board_manufacturer,
                                char * const board_product_name,
                                char * const board_serial_number,
                                char * const board_part_number,
                                char * const fru_file_id)
{
    size_t const board_manufacturer_len = strlen( board_manufacturer);
    size_t const board_product_name_len = strlen(board_product_name);
    size_t const board_serial_number_len = strlen(board_serial_number);
    size_t const board_part_number_len = strlen(board_part_number);
    size_t const fru_file_id_len = strlen(fru_file_id);

    int n = 11 + board_manufacturer_len + board_product_name_len + board_serial_number_len + board_part_number_len + fru_file_id_len;
    //      11 = 6 bytes for header and manufacturing date + 1 byte for each string added (5 in this case)

    // fix size of inventory board info to be multiple of 8 bytes
    if(n%8!=0)
    {
        n = n + (8-(n%8));
    }
    else n = n+8;

    // calculate area of board info field in multiple of 8 bytes
    uint8_t k = n;
    uint8_t s = 0;
    while(k>0)
    {
        k = k-8;
        s++;
    }

    size_t board_info_area = s; // BOARD INFO AREA IN MULTIPLES OF 8 BYTES

    board_info[0] = 0x01; // Board Area Format Version (01h standard specification)
    board_info[1] = board_info_area;
    board_info[2] = 25;   // Language set to: English

    board_info[3] = (uint8_t) ((mfg_date_time & 0x000000FF) >> 0); // DATE/TIME - Number of Minutes from 0:00 hrs 1/1/1996: 00 00 00h = unspecified ( LSbyte first, Little endian)
    board_info[4] = (uint8_t) ((mfg_date_time & 0x0000FF00) >> 8);
    board_info[5] = (uint8_t) ((mfg_date_time & 0x00FF0000) >> 16); // MSByte

    n = 6;
    int aux;
    int offset;

    board_info[n] = board_manufacturer_len | ASCII_CODE;
    n++;
    offset=n;
    aux=0;
    for(n=offset ; n < offset + board_manufacturer_len ; n++)
    {
       board_info[n] = board_manufacturer[aux]; // Insert first string in Board Info Field
       aux++;
    }

    board_info[n] = board_product_name_len | ASCII_CODE;
    n++;
    offset=n;
    aux=0;    
    for(n=offset ; n < offset+board_product_name_len ; n++)
    {
       board_info[n] = board_product_name[aux]; // Insert second string in Board Info Field
       aux++;
    }

    board_info[n] = board_serial_number_len | ASCII_CODE;
    n++;
    offset=n;
    aux=0;    
    for(n=offset ; n < offset+board_serial_number_len ; n++)
    {
       board_info[n] = board_serial_number[aux]; // Insert third string in Board Info Field
       aux++;
    }

    board_info[n] = board_part_number_len | ASCII_CODE;
    n++;
    offset=n;
    aux=0;    
    for(n=offset ; n < offset+board_part_number_len ; n++)
    {
       board_info[n] = board_part_number[aux]; // Insert fourth string in Board Info Field
       aux++;
    }

    board_info[n] = fru_file_id_len | ASCII_CODE;
    n++;
    offset=n;
    aux=0;    
    for(n=offset ; n < offset+fru_file_id_len ; n++)
    {
       board_info[n] = fru_file_id[aux]; // Insert last string in Board Info Field
       aux++;
    }

    board_info[n] = 0xc1; // END OF inventory_board_info

    if (n+1 < (board_info_area*8) )
    {
        for (aux=n+1; aux<=((board_info_area*8)-2) ; aux++)
            board_info[aux] = 0; // fill the non used spaces with zero
    }
    
    uint8_t board_area_checksum = 0x00;
    
    for (aux=0 ; aux<=((board_info_area*8)-2) ; aux++)
    {
        board_area_checksum += board_info[aux];
    }

    board_info[(board_info_area*8)-1] = (~board_area_checksum) +1;

    return board_info_area;
}


/*
 * ASSEMBLE FRU INVENTORY USING CREATED FIELDS
 */
void create_fru_inventory ( fru_inventory_field_t *internal_use,
                            fru_inventory_field_t *chassis_info,
                            fru_inventory_field_t *board_info,
                            fru_inventory_field_t *product_info,
                            fru_inventory_field_t *multi_record)
{
    size_t aux_size[] = {0,0,0,0,0};
    if (internal_use != NULL) {aux_size[0] = internal_use->field_area;}
    if (chassis_info != NULL) {aux_size[1] = chassis_info->field_area;}
    if (board_info   != NULL) {aux_size[2] = board_info->field_area;}
    if (product_info != NULL) {aux_size[3] = product_info->field_area;}
    if (multi_record != NULL) {aux_size[4] = multi_record->field_area;}

    fru_inventory_size = ( 1 + aux_size[0] + aux_size[1] + aux_size[2] + aux_size[3])*8 + aux_size[4];
    fru_inventory = pvPortMalloc( sizeof(uint8_t)*fru_inventory_size );

    /*
     * HEADER ASSMBLE (size of header is 8 bytes fixed)
     */
    fru_inventory[0] = 0x01; // Format Version Number - 01h for this specification

    // Insert Fields OFFSET in Inventory (multiples of 8 bytes)
    // Check if Field is created by designated function (if pointer is NULL field is not created)
    int header_offset = 0;

    if(internal_use == NULL)
    {fru_inventory[1] = 0;}
    else { fru_inventory[1] = 1 + header_offset; header_offset += internal_use->field_area;}

    if(chassis_info == NULL)
    {fru_inventory[2] = 0;}
    else { fru_inventory[2] = 1 + header_offset; header_offset += chassis_info->field_area;}

    if(board_info == NULL)
    {fru_inventory[3] = 0;}
    else { fru_inventory[3] = 1 + header_offset; header_offset += board_info->field_area;}

    if(product_info == NULL)
    {fru_inventory[4] = 0;}
    else { fru_inventory[4] = 1 + header_offset; header_offset += product_info->field_area;}

    if(multi_record == NULL)
    {fru_inventory[5] = 0;}
    else { fru_inventory[5] = 1 + header_offset;}

    fru_inventory[6] = 0x00; // Reserved write as 00h

    uint8_t header_checksum = 0x00;
    for (int aux=0 ; aux<7 ; aux++)
    {
        header_checksum += fru_inventory[aux];
    }
    fru_inventory[7] = (~header_checksum) +1;
    // END OF HEADER

    /*
     * FIELD DATA INSERTION IN INVENTORY
     */
    int i;
    int n;
    int offset = 8;

    // Internal Use Field
    if(internal_use != NULL)
    {
        i=0;
        for(n=0 ; n<(internal_use->field_area*8) ; n++)
        {
            fru_inventory[n + offset] = internal_use->field_ptr[i];
            i++;
        }
        offset += (internal_use->field_area*8);
    }

    // Chassis Info Field
    if(chassis_info != NULL)
    {
        i=0;
        for(n=0 ; n<(chassis_info->field_area*8) ; n++)
        {
            fru_inventory[n + offset] = chassis_info->field_ptr[i];
            i++;
        }
        offset += (chassis_info->field_area*8);
    }

    // Board Info Field
    if(board_info != NULL)
    {
        i=0;
        for(n=0 ; n<(board_info->field_area*8) ; n++)
        {
            fru_inventory[n + offset] = board_info->field_ptr[i];
            i++;
        }
        offset += (board_info->field_area*8);
    }

    // Product Info Field
    if(product_info != NULL)
    {
        i=0;
        for(n=0 ; n<(product_info->field_area*8) ; n++)
        {
            fru_inventory[n + offset] = product_info->field_ptr[i];
            i++;
        }
        offset += (product_info->field_area*8);
    }

    // Multi Record Field
    if(multi_record != NULL)
    {
        i=0;
        for(n=0 ; n<multi_record->field_area ; n++)
        {
            fru_inventory[n + offset] = multi_record->field_ptr[i];
            i++;
        }
    }

    // END of Inventory Creation
}

/*
 * LOAD BUFFER WITH FRU INVENTORY DATA AND RETURN TO IPMB TO TRANSMIT
 */
void get_inventory_data ( size_t offset_in_record, size_t bytes_to_read, uint8_t fru_id, uint8_t record_data_buff[])
{
    if (fru_id == 0x00)
    {
        for (int n = 0 ; n < bytes_to_read; n++)
        {
            record_data_buff[n] = fru_inventory[n + offset_in_record];
        }
    }
}



