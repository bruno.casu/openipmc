/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file ipmi_msg_command_switch.c
 *
 * @authors Alison Franca Queiroz da Costa
 * @authors Andre Muller Cascadan
 * @authors Bruno Augusto Casu
 *
 * @brief  Command switch to direct IMPI requests to the specific response function.
 *
 * This file directs IPMI requests sent by the Shelf Manager based on the Network Function and Command (definition in IPMI v1.5 and PICMG v3.0).
 * As the command is identified, the request is directed to a response function that returns the response bytes to be sent through IPMB.
 */

#include "stdint.h"
#include "ipmi_msg_manager.h"

/*
 * This functions generates the response for each command.
 * They are defined in files dedicated for each Network Function
 */
void ipmi_cmd_get_device_id                 (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_picmg_prop                (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_set_event_rec                 (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_device_loc_record_id      (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_set_fru_activation            (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_reserve_device_sdr_repos      (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_device_sdr                (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_device_sdr_info           (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_compute_power_prop            (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_power_level               (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_set_power_level               (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_fru_inventory_area_info   (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_read_fru_data                 (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_set_port_state                (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_address_info              (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_set_port_state                (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_sensor_reading            (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_set_sensor_event_enable       (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_sensor_event_status       (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_shelf_address_info        (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_set_shelf_address_info        (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_fru_control                   (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_fru_control_capabilities      (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_fru_led_prop              (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_led_color_capab           (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_set_fru_led_state             (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_fru_led_state             (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_set_fru_activation_policy     (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_fru_activation_policy     (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);

static void ipmi_cmd_invalid_command               (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);

int ipmi_msg_solve_request_ipmb0(uint8_t  netfn,
                                 uint8_t  command,
                                 uint8_t* request_bytes,
                                 int      req_len,
                                 uint8_t* completion_code,
                                 uint8_t* response_bytes,
                                 int*     res_len)
{
    switch(netfn)
    {
    //  case 0x00: // Chassis
    //
    //  case 0x02: // Bridge
    //
        case 0x04: // Sensor/Event
            switch(command)
            {
                case 0x00: // Set Event Receiver
                    ipmi_cmd_set_event_rec(request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x20: // Get Device SDR Info
                    ipmi_cmd_get_device_sdr_info(request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x21: // Get Device SDR
                    ipmi_cmd_get_device_sdr(request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x22: // Reserve Device SDR Repository
                    ipmi_cmd_reserve_device_sdr_repos(request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x28: // Set Sensor Event Enable
                    ipmi_cmd_set_sensor_event_enable(request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x2B: // Get Sensor Event Status
                    ipmi_cmd_get_sensor_event_status(request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x2D: // Get Sensor Reading
                    ipmi_cmd_get_sensor_reading(request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                default:
                return 0;
            }
        break;

        case 0x06: // App
            switch(command)
            {
                case 0x01: // Get Device ID
                    ipmi_cmd_get_device_id(request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

            // case 0x04: // Get Self Test Results
            //     ipmi_cmd_get_self_test_results(request_bytes, req_len, response_bytes, res_len, completion_code);
            // break;
            
                default:
                return 0;
            }
        break;

    //	case 0x08: // Firmware
    //    
        case 0x0A: // Storage
            switch(command)
            {
                case 0x10: // Get FRU Inventory Area Info
                    ipmi_cmd_get_fru_inventory_area_info (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x11: // Read FRU Data
                    ipmi_cmd_read_fru_data (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                default:
                return 0;
            }
        break;

    //  case 0x0C:  // Transport
    //  
        case 0x2C:  // PICMG
            switch(command)
            {
                case 0x00: // Get PICMG Properties
                    ipmi_cmd_get_picmg_prop(request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x01: // Get Address Info
                    ipmi_cmd_get_address_info (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x02: // Get Shelf Address Info
                    ipmi_cmd_get_shelf_address_info (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x03: // Set Shelf Address Info
                    ipmi_cmd_set_shelf_address_info (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x04: // FRU Control
                    ipmi_cmd_fru_control (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x05: // Get FRU LED Properties
                    ipmi_cmd_get_fru_led_prop (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x06: // Get LED Color Capabilities
                    ipmi_cmd_get_led_color_capab (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x07: // Set FRU LED State
                    ipmi_cmd_set_fru_led_state (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x08: // Get FRU LED State
                    ipmi_cmd_get_fru_led_state (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x0A: // Set FRU Activation Policy
                    ipmi_cmd_set_fru_activation_policy (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x0B: // Get FRU Activation Policy
                    ipmi_cmd_get_fru_activation_policy (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x0C: // Set FRU Activation
                    ipmi_cmd_set_fru_activation (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x0D: // Get Device Locator Record ID
                    ipmi_cmd_get_device_loc_record_id (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x0E: // Set Port State (not supported)
                    ipmi_cmd_set_port_state (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x10: // Compute Power Properties
                    ipmi_cmd_compute_power_prop (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x11: // Set Power Level
                    ipmi_cmd_set_power_level (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x12: // Get Power Level
                    ipmi_cmd_get_power_level (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x1E: // FRU Control Capabilities
                    ipmi_cmd_fru_control_capabilities (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x3E: // UNKNOWN CMD 3E (not supported)
                    ipmi_cmd_invalid_command (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                default:
                return 0;
            }
        break;

        default:
        return 0;
    }
    return 1;
}

/**
 * @brief Specific function to provide a response for non-implemented commands.
 *
 * If the user of the IPMC code encounter an unrecognized command sent by the Shelf Manager this
 * function can be implemented in the Switch/Case of ipmi_msg_solve_request_ipmb0(), in order to provide a 
 * proper response for unsupported commands to Shelf Manager.
 *
 * @param request_bytes   Data bytes in the IPMI request, directed by the solve request function
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (in this case no data bytes are returned). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (defined in IPMI v1.5).
 */
static void ipmi_cmd_invalid_command( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    *res_len = 0;
    *completion_code = 0xC1;     // Command Invalid (defined in IPMI v1.5) - unrecognized or unsupported command 
}






