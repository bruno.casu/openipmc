/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file sensors_templates.c
 * 
 * @author Bruno Augusto Casu
 * 
 * @brief  Template functions to create SDR
 *
 */

#include "string.h"
#include "stdint.h"

#include "ipmb_0.h"

#include "sdr_definitions.h"
#include "sdr_manager.h"

/*
 * Allocate record and Load with standard info for Temperature sensor (SDR Type 01h)
 * Sensor number is configured to be the same as the allocated record id
 */
void create_simple_temperature_sensor (uint8_t base_unit_type, uint8_t linearization, uint16_t m, uint16_t b, uint16_t accuracy, uint8_t non_recoverable_th, uint8_t critical_th, uint8_t non_critical_th, char* id_string, void (*get_sensor_reading_func)(sensor_reading_t*))
{
    uint16_t const record_id = allocate_sdr (FULL_SENSOR_RECORD);
    uint8_t  const sensor_number = (uint8_t) ((record_id & 0x00FF) >> 0);
    sdr_type_01_t* record = allrecords[record_id].ptr;
    allrecords[record_id].get_sensor_reading_func = get_sensor_reading_func;

    //Header
    record->record_id =                                        record_id;
    record->sdr_version =                                      IPMI_VERSION_1_5;
    record->record_type_number =                               RECORD_TYPE_01H;
    //Key Bytes
    record->sensor_owner_id =                                  NOT_USED_FIELD; 
    record->channel_number =                                   NOT_USED_FIELD;
    record->sensor_owner_lun =                                 LUN_O;
    record->sensor_number =                                    sensor_number;
    //Body
    record->entity_id =                                        BOARD_SPECIFIC_ENTITY;
    record->entity_instance =                                  PHYSICAL_ENTITY | DEVICE_RELATIVE_NUM_0;
        //Initialization
    record->initialization =                                   INIT_SCANNING_ENABLE | INIT_EVENTS_ENABLE | INIT_THRESHOLDS_ENABLE | INIT_SENSOR_TYPE_ENABLE;
    record->sensor_default_power_up_state =                    EVENT_GENERATION_ENABLED | SENSOR_SCANNING_ENABLED;
        //Capabilities
    record->sensor_status_check =                              DO_NOT_IGNORE_SENSOR;
    record->sensor_auto_rearm =                                AUTO_REARM_YES;
    record->sensor_hysteresis_support =                        NO_HYSTERESIS;
    record->sensor_threshold_access_support =                  READBLE_SETTABLE_TH;
    record->sensor_event_msg_ctrl_support =                    EVENT_PER_THRESHOLD;
    record->sensor_type =                                      TEMPERATURE;
    record->event_reading_type =                               EVENT_READING_THRESHOLD;
        //Masks
    record->assertion_event_mask_for_non_threshold_sensor =    NOT_USED_FIELD;
    record->lower_threshold_reading_mask =                     NOT_USED_FIELD; 
    record->threshold_assertion_event_mask =                   NOT_USED_FIELD;
    record->deassertion_event_mask_for_non_threshold_sensor =  NOT_USED_FIELD;
    record->upper_threshold_reading_mask =                     UPPER_NON_RECOVERABLE_TH_COMPARISON_RETURNED | UPPER_CRITICAL_TH_COMPARISON_RETURNED | UPPER_NON_CRITICAL_TH_COMPARISON_RETURNED;
    record->threshold_deassertion_event_mask =                 NOT_USED_FIELD;
    record->reading_mask_for_non_threshold_sensor =            NOT_USED_FIELD;
    record->settable_threshold_mask =                          NOT_USED_FIELD;
    record->readable_threshold_mask =                          NOT_USED_FIELD;
        //Data Format
    record->analog_data_format =                               UNSIGNED_DATA;
    record->rate_unit =                                        NOT_USED_FIELD;
    record->modifier_unit =                                    NOT_USED_FIELD;
    record->percentage =                                       NOT_USED_FIELD;
    record->sensor_base_unit =                                 base_unit_type;
    record->sensor_modifier_unit =                             NOT_USED_FIELD;
    record->linearization =                                    linearization;
    record->m =                                                m;
    record->tolerance =                                        NOT_USED_FIELD;
    record->b =                                                b;
    record->accuracy =                                         accuracy; 
    record->accuracy_exp =                                     NOT_USED_FIELD;
    record->r_exp =                                            NOT_USED_FIELD;
    record->b_exp =                                            NOT_USED_FIELD;
        // Ranges
    record->analog_characteristics =                           NOT_USED_FIELD;
    record->nominal_reading =                                  NOT_USED_FIELD;
    record->normal_maximum =                                   NOT_USED_FIELD;
    record->normal_minimum =                                   NOT_USED_FIELD;
    record->sensor_maximum_reading =                           0xff;
    record->sensor_minimum_reading =                           0x00;
        //Threshold Settings
    record->upper_non_recoverable_threshold =                  non_recoverable_th;
    record->upper_critical_threshold =                         critical_th;
    record->upper_non_critical_threshold =                     non_critical_th;
    record->lower_non_recoverable_threshold =                  NOT_USED_FIELD;
    record->lower_critical_threshold =                         NOT_USED_FIELD;
    record->lower_non_critical_threshold =                     NOT_USED_FIELD;
    record->positive_threshold_hysteresis_value =              NOT_USED_FIELD;
    record->negative_threshold_hysteresis_value =              NOT_USED_FIELD;
        //ID String Config
    record->id_string_type =                                   ASCII_FORMAT;
    record->id_string_length =                                 strlen(id_string);
    record->id_string =                                        id_string;
}

/*
 * Allocate record and Load with standard info for the Hot Swap Carrier (SDR Type 01h)
 * Sensor number is configured to be the same as the allocated record id
 */
void create_hot_swap_carrier_sensor (char* id_string)
{
    uint16_t const record_id = allocate_sdr (FULL_SENSOR_RECORD);
    uint8_t  const sensor_number = (uint8_t) ((record_id & 0x00FF) >> 0);
    sdr_type_01_t* record = allrecords[record_id].ptr;
        
    //Header                                                                                            
    record->record_id =                                        record_id;                               
    record->sdr_version =                                      IPMI_VERSION_1_5;                        
    record->record_type_number =                               RECORD_TYPE_01H;                         
    //Key Bytes                                                                                         
    record->sensor_owner_id =                                  NOT_USED_FIELD;                                          
    record->channel_number =                                   NOT_USED_FIELD;                          
    record->sensor_owner_lun =                                 LUN_O;                                   
    record->sensor_number =                                    sensor_number;                           
    //Body                                                                                              
    record->entity_id =                                        BOARD_SPECIFIC_ENTITY;                 
    record->entity_instance =                                  PHYSICAL_ENTITY | DEVICE_RELATIVE_NUM_0;                                           
        //Initialization                                                                              
    record->initialization =                                   INIT_SCANNING_ENABLE | INIT_EVENTS_ENABLE | INIT_THRESHOLDS_ENABLE | INIT_SENSOR_TYPE_ENABLE | INIT_SENSOR_TYPE_ENABLE;                                    
    record->sensor_default_power_up_state =                    EVENT_GENERATION_ENABLED | SENSOR_SCANNING_ENABLED;                              
        //Capabilities                                                                                 
    record->sensor_status_check =                              DO_NOT_IGNORE_SENSOR;                   
    record->sensor_auto_rearm =                                AUTO_REARM_YES;                         
    record->sensor_hysteresis_support =                        NO_HYSTERESIS;                          
    record->sensor_threshold_access_support =                  NO_THRESHOLDS;                          
    record->sensor_event_msg_ctrl_support =                    ENTIRE_SENSOR_ONLY;                     
    record->sensor_type =                                      HOT_SWAP_CARRIER;                       
    record->event_reading_type =                               EVENT_READING_SPECIFIC;                 
        //Masks                                                                                        
    record->assertion_event_mask_for_non_threshold_sensor =    NOT_USED_FIELD;                         
    record->lower_threshold_reading_mask =                     NOT_USED_FIELD;                         
    record->threshold_assertion_event_mask =                   NOT_USED_FIELD;                         
    record->deassertion_event_mask_for_non_threshold_sensor =  NOT_USED_FIELD;                         
    record->upper_threshold_reading_mask =                     NOT_USED_FIELD;                         
    record->threshold_deassertion_event_mask =                 NOT_USED_FIELD;                         
    record->reading_mask_for_non_threshold_sensor =            NOT_USED_FIELD;                         
    record->settable_threshold_mask =                          NOT_USED_FIELD;                         
    record->readable_threshold_mask =                          NOT_USED_FIELD;                         
        //Data Format                                                                                  
    record->analog_data_format =                               NOT_USED_FIELD;                         
    record->rate_unit =                                        NOT_USED_FIELD;                         
    record->modifier_unit =                                    NOT_USED_FIELD;                         
    record->percentage =                                       NOT_USED_FIELD;                         
    record->sensor_base_unit =                                 NOT_USED_FIELD;                         
    record->sensor_modifier_unit =                             NOT_USED_FIELD;                         
    record->linearization =                                    NOT_USED_FIELD;                         
    record->m =                                                NOT_USED_FIELD;                         
    record->tolerance =                                        NOT_USED_FIELD;                         
    record->b =                                                NOT_USED_FIELD;                         
    record->accuracy =                                         NOT_USED_FIELD;                         
    record->accuracy_exp =                                     NOT_USED_FIELD;                         
    record->r_exp =                                            NOT_USED_FIELD;                         
    record->b_exp =                                            NOT_USED_FIELD;                         
        // Ranges                                                                                      
    record->analog_characteristics =                           NOT_USED_FIELD;                     
    record->nominal_reading =                                  0xd0;                                   
    record->normal_maximum =                                   NOT_USED_FIELD;                         
    record->normal_minimum =                                   NOT_USED_FIELD;                         
    record->sensor_maximum_reading =                           0xff;                     
    record->sensor_minimum_reading =                           0x00;                     
        //Threshold Settings                                                                           
    record->upper_non_recoverable_threshold =                  0xc0;                                   
    record->upper_critical_threshold =                         NOT_USED_FIELD;                         
    record->upper_non_critical_threshold =                     NOT_USED_FIELD;                         
    record->lower_non_recoverable_threshold =                  NOT_USED_FIELD;                         
    record->lower_critical_threshold =                         NOT_USED_FIELD;                          
    record->lower_non_critical_threshold =                     NOT_USED_FIELD;                          
    record->positive_threshold_hysteresis_value =              NOT_USED_FIELD;                          
    record->negative_threshold_hysteresis_value =              NOT_USED_FIELD;                          
        //ID String Config                                                                              
    record->id_string_type =                                   ASCII_FORMAT;                            
    record->id_string_length =                                 strlen(id_string);                       
    record->id_string =                                        id_string;                               
}                                                                                                       

/*
 * Allocate record and Load with standard info for the Main Board (SDR Type 12h)
 * IMPORTANT! the Management Controller Device Locator (SDR Type 12h) should be
 * configured to have record id 0001h, therefore it must be the first record to be created
 */
void create_management_controller_sdr (char* id_string)
{
    uint16_t const record_id = allocate_sdr (MANAGEMENT_CONTROLLER_DEVICE_LOCATOR);
    sdr_type_12_t* record = allrecords[record_id].ptr;

    //Header
    record->record_id =                                        record_id;
    record->sdr_version =                                      IPMI_VERSION_1_5;
    record->record_type_number =                               RECORD_TYPE_12H;
    //Key Bytes
    record->device_slave_address =                             ipmb_0_addr;
    record->channel_number =                                   NOT_USED_FIELD;
    // Body
    record->power_state_notification =                         SYS_PWR_STATE_NOTIF_REQUIRED | DEV_PWR_STATE_NOTIF_REQUIRED | STATIC_CONTROLLER;
    record->global_initialization =                            ENABLE_EVENT_MSG_GENERATION_FROM_CONTROLLER;
    record->device_capabilities =                              EVENT_GENERATOR_SUPPORTED | INVENTORY_DEVICE_SUPPORTED | SDR_REPOSITORY_DEVICE_SUPPORTED | SENSOR_DEVICE_SUPPORTED;
    record->entity_id =                                        BOARD_SPECIFIC_ENTITY;
    record->entity_instance =                                  ENTITY_INSTANCE_01;
    record->id_string_type =                                   ASCII_FORMAT;
    record->id_string_length =                                 strlen(id_string);
    record->id_string =                                        id_string; 
}

/*
 * Allocate record and Load with standard info for Voltage Out or VCC sensor (SDR Type 01h)
 * Sensor number is configured to be the same as the allocated record id
 */
void create_simple_voltage_out_sensor(uint8_t linearization, uint16_t m, uint16_t b, uint16_t accuracy, uint8_t r_exp, uint8_t upper_non_critical_th, uint8_t lower_non_critical_th, char* id_string, void (*get_sensor_reading_func)(sensor_reading_t*))
{
    uint16_t const record_id = allocate_sdr (FULL_SENSOR_RECORD);
    uint8_t  const sensor_number = (uint8_t) ((record_id & 0x00FF) >> 0);
    sdr_type_01_t* record = allrecords[record_id].ptr;
    allrecords[record_id].get_sensor_reading_func = get_sensor_reading_func;

   //Header
    record->record_id =                                        record_id;
    record->sdr_version =                                      IPMI_VERSION_1_5;
    record->record_type_number =                               RECORD_TYPE_01H;
    //Key Bytes
    record->sensor_owner_id =                                  NOT_USED_FIELD; 
    record->channel_number =                                   NOT_USED_FIELD;
    record->sensor_owner_lun =                                 LUN_O;
    record->sensor_number =                                    sensor_number;
    //Body
    record->entity_id =                                        BOARD_SPECIFIC_ENTITY;
    record->entity_instance =                                  PHYSICAL_ENTITY | DEVICE_RELATIVE_NUM_0;
        //Initialization
    record->initialization =                                   INIT_SCANNING_ENABLE | INIT_EVENTS_ENABLE | INIT_THRESHOLDS_ENABLE | INIT_SENSOR_TYPE_ENABLE;
    record->sensor_default_power_up_state =                    EVENT_GENERATION_ENABLED | SENSOR_SCANNING_ENABLED;
        //Capabilities
    record->sensor_status_check =                              DO_NOT_IGNORE_SENSOR;
    record->sensor_auto_rearm =                                AUTO_REARM_YES;
    record->sensor_hysteresis_support =                        NO_HYSTERESIS;
    record->sensor_threshold_access_support =                  READBLE_SETTABLE_TH;
    record->sensor_event_msg_ctrl_support =                    EVENT_PER_THRESHOLD;
    record->sensor_type =                                      VOLTAGE;
    record->event_reading_type =                               EVENT_READING_THRESHOLD;
        //Masks
    record->assertion_event_mask_for_non_threshold_sensor =    NOT_USED_FIELD;
    record->lower_threshold_reading_mask =                     NOT_USED_FIELD; 
    record->threshold_assertion_event_mask =                   NOT_USED_FIELD;
    record->deassertion_event_mask_for_non_threshold_sensor =  NOT_USED_FIELD;
    record->upper_threshold_reading_mask =                     UPPER_NON_RECOVERABLE_TH_COMPARISON_RETURNED | UPPER_CRITICAL_TH_COMPARISON_RETURNED | UPPER_NON_CRITICAL_TH_COMPARISON_RETURNED;
    record->threshold_deassertion_event_mask =                 NOT_USED_FIELD;
    record->reading_mask_for_non_threshold_sensor =            NOT_USED_FIELD;
    record->settable_threshold_mask =                          NOT_USED_FIELD;
    record->readable_threshold_mask =                          NOT_USED_FIELD;
        //Data Format
    record->analog_data_format =                               UNSIGNED_DATA;
    record->rate_unit =                                        NOT_USED_FIELD;
    record->modifier_unit =                                    NOT_USED_FIELD;
    record->percentage =                                       NOT_USED_FIELD;
    record->sensor_base_unit =                                 VOLTS;
    record->sensor_modifier_unit =                             NOT_USED_FIELD;
    record->linearization =                                    linearization;
    record->m =                                                m;
    record->tolerance =                                        NOT_USED_FIELD;
    record->b =                                                b;
    record->accuracy =                                         accuracy; 
    record->accuracy_exp =                                     NOT_USED_FIELD;
    record->r_exp =                                            r_exp;
    record->b_exp =                                            NOT_USED_FIELD;
        // Ranges
    record->analog_characteristics =                           NOT_USED_FIELD;
    record->nominal_reading =                                  NOT_USED_FIELD;
    record->normal_maximum =                                   NOT_USED_FIELD;
    record->normal_minimum =                                   NOT_USED_FIELD;
    record->sensor_maximum_reading =                           0xff;
    record->sensor_minimum_reading =                           0x00;
        //Threshold Settings
    record->upper_non_recoverable_threshold =                  NOT_USED_FIELD;
    record->upper_critical_threshold =                         NOT_USED_FIELD;
    record->upper_non_critical_threshold =                     upper_non_critical_th;
    record->lower_non_recoverable_threshold =                  NOT_USED_FIELD;
    record->lower_critical_threshold =                         NOT_USED_FIELD;
    record->lower_non_critical_threshold =                     lower_non_critical_th;
    record->positive_threshold_hysteresis_value =              NOT_USED_FIELD;
    record->negative_threshold_hysteresis_value =              NOT_USED_FIELD;
        //ID String Config
    record->id_string_type =                                   ASCII_FORMAT;
    record->id_string_length =                                 strlen(id_string);
    record->id_string =                                        id_string;
}



