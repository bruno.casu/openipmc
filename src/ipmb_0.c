/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file ipmb_0.c
 * 
 * @authors Alison Franca Queiroz da Costa
 * @authors Andre Muller Cascadan
 * 
 * @brief  Management tasks for incoming and outgoing messages from IPMB-A and IPMB-B.
 *
 * @sa {@link ipmb_0.h } 
 */

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

#include "stdlib.h"
#include "string.h"
#include "stdint.h"

#include "ipmb_0.h"
#include "ipmc_ios.h"

#define DELAY_1_SECOND		1000UL


// IPMB address in 8bit format.
uint8_t ipmb_0_addr;


/*
 * IPMI message queues.
 * 
 * All incoming and outgoing messages trough IPMB-0
 * are put in these queues. They are the interface to 
 * this bus.
 */
QueueHandle_t queue_ipmb0_req_in = NULL;
QueueHandle_t queue_ipmb0_res_in = NULL;
QueueHandle_t queue_ipmb0_out    = NULL;



TaskHandle_t ipmb_0_msg_receiver_task_ptr;
TaskHandle_t ipmb_0_msg_sender_task_ptr;




// Print bytes for debug 
static void debug_print(const char *prefix, uint8_t data[], int len)
{
	char bytes[100];
	char byte[3];
	int i;
	
	bytes[0] = 0;
	for (i=0; i<len; i++)
	{
		itoa(data[i], byte, 16);
		if(data[i] <= 0x0F)
			strcat(bytes, "0"); // add zero to left
		strcat(bytes, byte);
		strcat(bytes, " ");
	}
	
	ipmc_ios_printf("%s: %s\r\n", prefix, bytes);
}


/*
 * Task to manage the incoming messages from IPMB-A and
 * IPMB-B.
 * 
 * It is responsible for getting messages from both IPMB
 * channels and discriminate them as response or request.
 * 
 */
void ipmb_0_msg_receiver_task( void *pvParameters )
{

	data_ipmb senddataA;
	data_ipmb senddataB;
	uint8_t ReadBuffer[100];
	int len;
	int i;
	uint8_t header_checksum;
	
	// Create the input queues
	queue_ipmb0_req_in = xQueueCreate( 5, sizeof( data_ipmb ) );
	queue_ipmb0_res_in = xQueueCreate( 5, sizeof( data_ipmb ) );

	// Wait until ipmc_ios be ready!
	while ( ipmc_ios_ready() != pdTRUE )
	{
		vTaskDelay( pdMS_TO_TICKS(100) );
	}


		for( ;; )
		{

			// Blocks until receive something or timeout
			ipmc_ios_ipmb_wait_input_msg();
			

			len = ipmc_ios_ipmba_read(ReadBuffer);
			header_checksum = ReadBuffer[0] + ReadBuffer[1] + ReadBuffer[2];

			if ( (len > 0) && (header_checksum==0) ){

				senddataA.channel = 'A';
				senddataA.length = len;
				//senddataA.data = pvPortMalloc(len);

				for (i=0; i<len; i++){
					senddataA.data[i] = ReadBuffer[i];
				}


				// Check the NetFn (byte 1) to decide the proper queue for this message.
				// Even means Request, odd means Response. The LSB of NetFn is in bit 2.
				if( (senddataA.data[1] & (1<<2)) == 0 )
					xQueueSendToBack(queue_ipmb0_req_in, &senddataA, 0UL);
				else
					xQueueSendToBack(queue_ipmb0_res_in, &senddataA, 0UL);
				
				debug_print( "IPMB-0 rcvd", senddataA.data, senddataA.length);
				
			}

			len = ipmc_ios_ipmbb_read(ReadBuffer);
			header_checksum = ReadBuffer[0] + ReadBuffer[1] + ReadBuffer[2];

			if ( (len > 0) && (header_checksum==0) ){

				senddataB.channel = 'B';
				senddataB.length = len;
				//senddataB.data = pvPortMalloc(len);

				for (i=0; i<len; i++){
					senddataB.data[i] = ReadBuffer[i];
				}


				// Check the NetFn (byte 1) to decide the proper queue for this message.
				// Even means Request, odd means Response. The LSB of NetFn is in bit 2.
				if( (senddataB.data[1] & (1<<2)) == 0 )
					xQueueSendToBack(queue_ipmb0_req_in, &senddataB, 0UL);
				else
					xQueueSendToBack(queue_ipmb0_res_in, &senddataB, 0UL);

				debug_print( "IPMB-0 rcvd", senddataB.data, senddataB.length);
				
			}

		}

}



/*
 * Task to manage the outgoing messages to IPMB-A and 
 * IPMB-B.
 * 
 * It is responsible for send the messages though both
 * channels using the proper arbitration rule.
 */
void ipmb_0_msg_sender_task( void *pvParameters )
{

	uint8_t read_addr;
	data_ipmb rec_data;
	char round_robin_state = 'A'; // 'A' or 'B'
	const int max_attempts = 3;
	int attempt;
	
	// Wait until ipmc_ios be ready!
	while ( ipmc_ios_ready() != pdTRUE )
		vTaskDelay( pdMS_TO_TICKS(100) );
	
	// Set the IPMB address to the IPMB interfaces.
	// The Hardware Address is converted into 8bit format and stored in a global.
	read_addr = ipmc_ios_read_haddress();
    if (read_addr != HA_PARITY_FAIL)
		ipmb_0_addr = read_addr << 1; // Normal address
	else
		ipmb_0_addr = IPMB_ERROR_ADDR; // Failure address
	ipmc_ios_ipmb_set_addr(ipmb_0_addr);
	
	// Create the output queue
	queue_ipmb0_out = xQueueCreate( 5, sizeof( data_ipmb ) );
	
	
	for( ;; )
	{

		// Gets a message from queue to be transmitted
		xQueueReceive(queue_ipmb0_out, &rec_data, portMAX_DELAY);
		int send_complete = pdFALSE;

		
		// Attempts to transmit
		for (attempt = 0; (attempt < max_attempts) && (send_complete == pdFALSE); attempt++)
		{
			
			if(round_robin_state == 'A')
			{
				round_robin_state = 'B'; // swap channel
				
				if( ipmc_ios_ipmba_send(rec_data.data, rec_data.length) != IPMB_SEND_FAIL )
					send_complete = pdTRUE;
				
			}
			else // IPMB-B
			{
				round_robin_state = 'A'; // swap channel
				
				if( ipmc_ios_ipmbb_send(rec_data.data, rec_data.length) != IPMB_SEND_FAIL )
					send_complete = pdTRUE;
				 
			}
			
		}
		
		
		// Debug output:
		// Print the result of transmission
		if(send_complete == pdTRUE)
			debug_print( "IPMB-0 sent", rec_data.data, rec_data.length);
		else
			ipmc_ios_printf("Transmission faild!\r\n");   // If fails all attempts
	}
}
