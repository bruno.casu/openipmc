/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file sensors_templates.h
 * 
 * @author Bruno Augusto Casu
 * 
 * @brief  Headers for the template functions to create SDR
 *
 */

#ifndef SENSORS_TEMPLATES_H
#define SENSORS_TEMPLATES_H

/**
 * @name Custom functions for SDR creation
 * @{
 */

/**
 * @brief User customizable function to create an SDR for a basic temperature sensor.
 * 
 * @param base_unit_type Unit for the temperature sensor (°C, °F, K).
 * @param linearization Mathematical function that describes sensor behavior (linear, log10, ln, etc...).
 * @param m Angular coefficient for the sensor curve ( Y = mX + b , X is the RAW value) [2’s complement, signed, 10 bit].
 * @param b Offset in the sensor measurement ( Y = mX + b , X is the RAW value) [2’s complement, signed, 10-bit].
 * @param accuracy Unsigned, 10-bit Basic Sensor Accuracy in 1/100 percent.
 * @param non_recoverable_th Upper non recoverable threshold, 8 bit RAW value (consider 'm' and 'b' parameters to calculate the real value). 
 * @param critical_th Upper critical threshold, 8 bit RAW value (consider 'm' and 'b' parameters to calculate the real value).
 * @param non_critical_th Upper non critical threshold, 8 bit RAW value (consider 'm' and 'b' parameters to calculate the real value).
 * @param id_string String to identify the sensor. Maximum of 16 ASCII characters.
 * @param get_sensor_reading_func Pointer to the reading function of the sensor.
 * 
 * This function is an example function that was designed to provide the OpenIPMC user a simple way to create a record for a temperature sensor.
 * Only few parameter of the SDR struct are required in the create function, which will fill automatically the rest of the SDR struct with predefined values.
 * User is free to adapt this function to enable customization for other fields.
 * 
 * Is important to mention that this function requires an SDR allocation before the parameters can be inserted in the struct. Therefore is mandatory
 * that the {@link allocate_sdr()} is called at the beginning of an SDR creation function, in order to provide an Record ID for it and store the SDR in the global struct.
 * 
 * @note This function creates an {@link sdr_type_01_t} struct with the inserted parameters.
 */
void create_simple_temperature_sensor (uint8_t base_unit_type, uint8_t linearization, uint16_t m, uint16_t b, uint16_t accuracy, uint8_t non_recoverable_th, uint8_t critical_th, uint8_t non_critical_th, char* id_string, void (*get_sensor_reading_func)(sensor_reading_t*));

/**
 * @brief Customizable function to create the Hot Swap Carrier sensor SDR.
 * 
 * @param id_string String to identify the sensor. Maximum of 16 ASCII characters.
 * 
 * This function creates the record for the Hot Swap Carrier sensor, and fill the SDR with predefined parameters, specific for the carrier sensor type.
 */
void create_hot_swap_carrier_sensor (char* id_string);

/**
 * @brief Customizable function to create ans SDR for the IPMC componenet
 * 
 * @param id_string String to identify the management controller. Maximum of 16 ASCII characters.
 * 
 * This function is used to set the parameters for the management controller unit. It provides the user a simple interface to create the mandatory {@link sdr_type_12_t}, which contains relevant information about
 * the IPMC (definitions in IPMI v1.5 and PICMG v3.0). A standard configuration is set in the create function, leaving only the id string as a customizabe parameter.
 */
void create_management_controller_sdr (char* id_string);

/**
 * @brief User customizable function to create an SDR for a voltage sensor.
 * 
 * @param linearization Mathematical function that describes sensor behavior (linear, log10, ln, etc...).
 * @param m Angular coefficient for the sensor curve ( Y = mX + b , X is the RAW value) [2’s complement, signed, 10 bit].
 * @param b Offset in the sensor measurement ( Y = mX + b , X is the RAW value) [2’s complement, signed, 10-bit].
 * @param accuracy Unsigned, 10-bit Basic Sensor Accuracy in 1/100 percent.
 * @param r_exp Result exponent (R = Y*10^r_exp) [2’s complement, signed, 4-bit]
 * @param upper_non_critical_th Upper non critical threshold, 8 bit RAW value (consider 'm' and 'b' parameters to calculate the real value).
 * @param lower_non_critical_th Lower non critical threshold, 8 bit RAW value (consider 'm' and 'b' parameters to calculate the real value).
 * @param id_string String to identify the sensor. Maximum of 16 ASCII characters.
 * @param get_sensor_reading_func Pointer to the reading function of the sensor.
 * 
 * This function is an example function that was designed to provide the OpenIPMC user a simple way to create a record for a voltage sensor.
 * Only few parameter of the SDR struct are required in the create function, which will fill automatically the rest of the SDR struct with predefined values.
 * User is free to adapt this function to enable customization for other fields.
 * 
 * Is important to mention that this function requires an SDR allocation before the parameters can be inserted in the struct. Therefore is mandatory
 * that the {@link allocate_sdr()} is called at the beginning of an SDR creation function, in order to provide an Record ID for it and store the SDR in the global struct.
 * 
 * @note This function creates an {@link sdr_type_01_t} struct with the inserted parameters.
 */
void create_simple_voltage_out_sensor(uint8_t linearization, uint16_t m, uint16_t b, uint16_t accuracy, uint8_t r_exp, uint8_t upper_non_critical_th, uint8_t lower_non_critical_th, char* id_string, void (*get_sensor_reading_func)(sensor_reading_t*));

///@}
 
#endif // SENSORS_TEMPLATES_H
