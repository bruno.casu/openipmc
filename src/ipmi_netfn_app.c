/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file ipmi_netfn_app.c
 * 
 * @authors Alison Franca Queiroz da Costa
 * @authors Andre Muller Cascadan
 * @authors Bruno Augusto Casu
 * 
 * @brief  Response functions for Application Network Function commands.
 *
 * This file contains the specific functions to manage the requests of Application Network Function commands (defined in PICMG v3.0 and IPMI v1.5).
 * Each function returns the response data bytes and the completion code.
 * 
 * The specific functions are called by ipmi_msg_solve_request_ipmb0().    
 */

#include "stdint.h"
#include "ipmc_ios.h"


/**
 * @{
 * @name Application NetFn commands
 */

/**
 * @brief Specific function to provide a response for "Get Device ID" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.                        
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_get_device_id( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    ipmc_ios_printf("Get Device ID\r\n");

    response_bytes[0] = 0x01;	// Device ID
    
    response_bytes[1] = 0x80;   // [7]   1 = device provides Device SDRs 
                                // [6:4] Reserved
                                // [3:0] Device Revision (binary)

    response_bytes[2] = 0x01;   // [7]   0 = Device available: normal operation
                                // [6:0] Major Firmware Revision (binary) 
    
    response_bytes[3] = 0x00;   // Minor Firmware Revision (BCD)
    
    response_bytes[4] = 0x51;   // IPMI Version v1.5 (bits 7:4 holds the Least Significant digit, bits 3:0 holds the Most Significant digit)
    
    response_bytes[5] = 0x29;   // Additional Device Support (1b if function is supported)
                                // [7] Chassis Device 
                                // [6] Bridge
                                // [5] IPMB Event Generator
                                // [4] IPMB Event Receiver
                                // [3] FRU Inventory Device
                                // [2] SEL Device
                                // [1] SDR Repository Device
                                // [0] Sensor Device

    response_bytes[6] = 0x00;   // Manufacturer ID: PICMG 00315Ah (all codes in IANA PRIVATE ENTERPRISE NUMBERS)
    response_bytes[7] = 0x31;
    response_bytes[8] = 0x5a;

    response_bytes[9]  = 0x70;  // Product ID (LSB first)
    response_bytes[10] = 0x10;

    response_bytes[11] = 0x0a;  // Auxiliary Firmware Revision (optional field, IF THIS FIELD IS NOT USED req_len VALUE MUST BE CHANGED)
    response_bytes[12] = 0x00;
    response_bytes[13] = 0x00;
    response_bytes[14] = 0x00;

    *res_len = 15;
    *completion_code = 0; //OK
}

///@}
