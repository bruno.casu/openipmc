/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file sdr_definitions.h
 * 
 * @author Bruno Augusto Casu
 * 
 * @brief  Structure definitions for the Sensor Data Records (SDRs) in the IPMC.
 *
 * This header file has the Macros for the SDR parameters and the structs for each SDR Type (defined in IPMI v1.5)
 * The Macros are used for each data field in the SDRs structs.
 */

#ifndef SDR_DEFINITIONS_H
#define SDR_DEFINITIONS_H

/**
 * @name Macros used in the creation of SDRs.
 * @brief Each segment of macros is to be used for the individual fields in {@link sensors_templates.c} structs.
 * @{
 */

// SDR TYPE 01h and 02h

#define NOT_USED_FIELD              0x00

/**
 * @name sdr_version
 * @{
 */
#define IPMI_VERSION_1_5            0x51
///@}

/**
 * @name record_type_number
 * @{
 */
#define RECORD_TYPE_01H             0x01
#define RECORD_TYPE_02H             0x02
#define RECORD_TYPE_12H             0x12
///@}

/**
 * @name sensor_owner_id
 * @{
 */
#define ID_IS_IPMB_SLAVE_ADDRESS    0x00
#define SYSTEM_SOFTWARE_ID          0x01
///@}

/**
 * @name sensor_owner_lun
 * @{
 */
#define LUN_O                       0x00
#define LUN_1                       0x01
#define LUN_2                       0x02
#define LUN_3                       0x03
///@}

/**
 * @name entity_id
 * @{
 */
#define CHASSIS_SPECIFIC_ENTITY     0xa0 // (FOR ALL ENTITY ID CODES CONSULT: IPMI V1.5 table 37-12)
#define BOARD_SPECIFIC_ENTITY       0xb0
///@}

/**
 * @name entity_instance
 * @{
 */
#define PHYSICAL_ENTITY             0x00
#define LOGICAL_CONTAINER_ENTITY    0x80

#define SYSTEM_RELATIVE_NUM_0       0x00 // (FOR MORE INFO CONSULT: IPMI V1.5 table 33-1)
#define DEVICE_RELATIVE_NUM_0       0x60
///@}

/**
 * @name initialization
 * @{
 */
#define INIT_SCANNING_ENABLE        0x40
#define INIT_EVENTS_ENABLE          0x20
#define INIT_THRESHOLDS_ENABLE      0x10
#define INIT_HYSTERESIS_ENABLE      0x08
#define INIT_SENSOR_TYPE_ENABLE     0x04
///@}

/**
 * @name sensor_default_power_up_state
 * @{
 */
#define EVENT_GENERATION_ENABLED    0x02
#define SENSOR_SCANNING_ENABLED     0x01
///@}

/**
 * @name sensor_status_check
 * @{
 */
#define IGNORE_SENSOR_IF_ENTITY_DISABLED    0x80
#define DO_NOT_IGNORE_SENSOR                0x00
///@}

/**
 * @name sensor_auto_rearm
 * @{
 */
#define AUTO_REARM_YES              0x40
#define AUTO_REARM_NO               0x00
///@}

/**
 * @name sensor_hysteresis_support
 * @{
 */
#define NO_HYSTERESIS               0x00
#define READBLE_HYS                 0x10
#define READBLE_SETTABLE_HYS        0X20
#define HYS_FIXED_VALUES            0X30
///@}

/**
 * @name sensor_threshold_support
 * @{
 */
#define NO_THRESHOLDS               0x00
#define READBLE_TH                  0x04
#define READBLE_SETTABLE_TH         0x08
#define TH_FIXED_VALUES             0x0c
///@}

/**
 * @name sensor_event_msg_ctrl_support
 * @{
 */ 
#define EVENT_PER_THRESHOLD         0x00
#define ENTIRE_SENSOR_ONLY          0x01
#define GLOBAL_DISBLE_ONLY          0x02
#define NO_EVENT_FROM_SENSOR        0x03
///@}

/**
 * @name sensor_type
 * @{
 */
#define TEMPERATURE                 0X01 // (FOR ALL SENSOR TYPE CODES CONSULT: IPMI V1.5 table 36-3)
#define VOLTAGE                     0X02
#define CURRENT                     0X03
#define FAN                         0X04
#define HOT_SWAP_CARRIER            0Xf0
///@}

/**
 * @name event_reading_type
 * @{
 */
#define EVENT_READING_THRESHOLD     0x01 // (FOR ALL EVENT READING TYPE CODES CONSULT: IPMI V1.5 table 36-1)
#define EVENT_READING_SPECIFIC      0x6f
///@}

/**
 * @name lower_threshold_reading_mask
 * @{
 */
#define LOWER_NON_RECOVERABLE_TH_COMPARISON_RETURNED   0x40
#define LOWER_CRITICAL_TH_COMPARISON_RETURNED          0x20
#define LOWER_NON_CRITICAL_TH_COMPARISON_RETURNED      0x10
///@}

/**
 * @name threshold_assertion_event_mask
 * @{
 */
#define ASSERTION_EVENT_UPPER_NON_REC_TH_GOING_HIGH_SUPPORTED       0x0800  // ASSERTION EVENTS FOR UPPER NON RECOVERABLE TH GOING HIGH SUPPORTED
#define ASSERTION_EVENT_UPPER_NON_REC_TH_GOING_LOW_SUPPORTED        0x0400  // ASSERTION EVENTS FOR UPPER NON RECOVERABLE TH GOING LOW SUPPORTED
#define ASSERTION_EVENT_UPPER_CRITICAL_TH_GOING_HIGH_SUPPORTED      0x0200  // ASSERTION EVENTS FOR UPPER CRITICAL TH GOING HIGH SUPPORTED
#define ASSERTION_EVENT_UPPER_CRITICAL_TH_GOING_LOW_SUPPORTED       0x0100  // ASSERTION EVENTS FOR UPPER CRITICAL TH GOING LOW SUPPORTED
#define ASSERTION_EVENT_UPPER_NON_CRIT_TH_GOING_HIGH_SUPPORTED      0x0080  // ASSERTION EVENTS FOR UPPER NON CRITICAL TH GOING HIGH SUPPORTED
#define ASSERTION_EVENT_UPPER_NON_CRIT_TH_GOING_LOW_SUPPORTED       0x0040  // ASSERTION EVENTS FOR UPPER NON CRITICAL TH GOING LOW SUPPORTED
#define ASSERTION_EVENT_LOWER_NON_REC_TH_GOING_HIGH_SUPPORTED       0x0020  // ASSERTION EVENTS FOR LOWER NON RECOVERABLE TH SUPPORTED
#define ASSERTION_EVENT_LOWER_NON_REC_TH_GOING_LOW_SUPPORTED        0x0010  // ASSERTION EVENTS FOR LOWER NON RECOVERABLE TH SUPPORTED
#define ASSERTION_EVENT_LOWER_CRITICAL_TH_GOING_HIGH_SUPPORTED      0x0008  // ASSERTION EVENTS FOR LOWER CRITICAL TH SUPPORTED
#define ASSERTION_EVENT_LOWER_CRITICAL_TH_GOING_LOW_SUPPORTED       0x0004  // ASSERTION EVENTS FOR LOWER CRITICAL TH SUPPORTED
#define ASSERTION_EVENT_LOWER_NON_CRIT_TH_GOING_HIGH_SUPPORTED      0x0002  // ASSERTION EVENTS FOR LOWER NON CRITICAL TH SUPPORTED
#define ASSERTION_EVENT_LOWER_NON_CRIT_TH_GOING_LOW_SUPPORTED       0x0001  // ASSERTION EVENTS FOR LOWER NON CRITICAL TH SUPPORTED
///@}

/**
 * @name upper_threshold_reading_mask
 * @{
 */
#define UPPER_NON_RECOVERABLE_TH_COMPARISON_RETURNED   0x40
#define UPPER_CRITICAL_TH_COMPARISON_RETURNED          0x20
#define UPPER_NON_CRITICAL_TH_COMPARISON_RETURNED      0x10
///@}

/**
 * @name threshold_deassertion_event_mask
 * @{
 */
#define DEASSERTION_EVENT_UPPER_NON_REC_TH_GOING_HIGH_SUPPORTED     0x0800  // DEASSERTION EVENTS FOR UPPER NON RECOVERABLE TH GOING HIGH SUPPORTED
#define DEASSERTION_EVENT_UPPER_NON_REC_TH_GOING_LOW_SUPPORTED      0x0400  // DEASSERTION EVENTS FOR UPPER NON RECOVERABLE TH GOING LOW SUPPORTED
#define DEASSERTION_EVENT_UPPER_CRITICAL_TH_GOING_HIGH_SUPPORTED    0x0200  // DEASSERTION EVENTS FOR UPPER CRITICAL TH GOING HIGH SUPPORTED
#define DEASSERTION_EVENT_UPPER_CRITICAL_TH_GOING_LOW_SUPPORTED     0x0100  // DEASSERTION EVENTS FOR UPPER CRITICAL TH GOING LOW SUPPORTED
#define DEASSERTION_EVENT_UPPER_NON_CRIT_TH_GOING_HIGH_SUPPORTED    0x0080  // DEASSERTION EVENTS FOR UPPER NON CRITICAL TH GOING HIGH SUPPORTED
#define DEASSERTION_EVENT_UPPER_NON_CRIT_TH_GOING_LOW_SUPPORTED     0x0040  // DEASSERTION EVENTS FOR UPPER NON CRITICAL TH GOING LOW SUPPORTED
#define DEASSERTION_EVENT_LOWER_NON_REC_TH_GOING_HIGH_SUPPORTED     0x0020  // DEASSERTION EVENTS FOR LOWER NON RECOVERABLE TH SUPPORTED
#define DEASSERTION_EVENT_LOWER_NON_REC_TH_GOING_LOW_SUPPORTED      0x0010  // DEASSERTION EVENTS FOR LOWER NON RECOVERABLE TH SUPPORTED
#define DEASSERTION_EVENT_LOWER_CRITICAL_TH_GOING_HIGH_SUPPORTED    0x0008  // DEASSERTION EVENTS FOR LOWER CRITICAL TH SUPPORTED
#define DEASSERTION_EVENT_LOWER_CRITICAL_TH_GOING_LOW_SUPPORTED     0x0004  // DEASSERTION EVENTS FOR LOWER CRITICAL TH SUPPORTED
#define DEASSERTION_EVENT_LOWER_NON_CRIT_TH_GOING_HIGH_SUPPORTED    0x0002  // DEASSERTION EVENTS FOR LOWER NON CRITICAL TH SUPPORTED
#define DEASSERTION_EVENT_LOWER_NON_CRIT_TH_GOING_LOW_SUPPORTED     0x0001  // DEASSERTION EVENTS FOR LOWER NON CRITICAL TH SUPPORTED
///@}

/**
 * @name settable_threshold_mask
 * @{
 */
#define UPPER_NON_REC_TH_SETTABLE   0x20    // UPPER NON RECOVERABLE TH IS SETTABLE
#define UPPER_CRITICAL_TH_SETTABLE  0x10    // UPPER CRITICAL IS SETTABLE
#define UPPER_NON_CRIT_TH_SETTABLE  0x08    // UPPER NON CRITICAL IS SETTABLE
#define LOWER_NON_REC_TH_SETTABLE   0x04    // LOWER NON RECOVERABLE IS SETTABLE
#define LOWER_CRITICAL_TH_SETTABLE  0x02    // LOWER CRITICAL IS SETTABLE
#define LOWER_NON_CRIT_TH_SETTABLE  0x01    // LOWER NON CRITICAL IS SETTABLE
///@}

/**
 * @name readable_threshold_mask
 * @{
 */
#define UPPER_NON_REC_TH_READABLE   0x20    // UPPER NON RECOVERABLE TH IS READABLE
#define UPPER_CRITICAL_TH_READABLE  0x10    // UPPER CRITICAL IS READABLE
#define UPPER_NON_CRIT_TH_READABLE  0x08    // UPPER NON CRITICAL IS READABLE
#define LOWER_NON_REC_TH_READABLE   0x04    // LOWER NON RECOVERABLE IS READABLE
#define LOWER_CRITICAL_TH_READABLE  0x02    // LOWER CRITICAL IS READABLE
#define LOWER_NON_CRIT_TH_READABLE  0x01    // LOWER NON CRITICAL IS READABLE
///@}

/**
 * @name analog_data_format
 * @{
 */
#define UNSIGNED_DATA                   0x00
#define COMPLEMENT_OF_1                 0x40
#define COMPLEMENT_OF_2                 0x80
#define NOT_RETURN_ANALOG_READING       0xc0
///@}

/**
 * @name rate_unit
 * @{
 */
#define PER_MICRO_SEC                   0x08
#define PER_MILI_SEC                    0x10
#define PER_SEC                         0x18
#define PER_MINUTE                      0x20
#define PER_HOUR                        0x28
#define PER_DAY                         0x38
///@}

/**
 * @name modifier_unit
 * @{
 */
#define BASE_UNIT_DIV_MODIFIER_UNIT     0x02
#define BASE_UNIT_MUL_MODIFIER_UNIT     0x04
///@}

/**
 * @name percentage
 * @{
 */
#define PERCENTAGE_YES                  0x01
///@}

/**
 * @name sensor_base_unit and sensor_modifier_unit
 * @{
 */
#define UNSPECIFIED_UNIT                0x00 // (FOR ALL UNITS CODES CONSULT: IPMI V1.5 table 37-14)
#define DEGREES_C                       0x01 
#define DEGREES_F                       0x02
#define KELVIN                          0x03
#define VOLTS                           0x04
#define AMPERES                         0x05
#define WATTS                           0x06
#define STERADIANS                      0x28
///@}

/**
 * @name linearization
 * @{
 */
#define LINEAR                          0x00 // (FOR ALL OPTIONS CONSULT: IPMI V1.5 Full Sensor Record table, byte 24)
#define LOGN                            0x01
#define LOG10                           0x02
///@}

/**
 * @name analog_characteristics
 * @{
 */
#define ANALOG_NORMAL_MIN_SPECIFIED     0x04
#define ANALOG_NORMAL_MAX_SPECIFIED     0x02
#define NOMINAL_READING_SPECIFIED       0x01
///@}

/**
 * @name id_string_type
 * @{
 */
#define ASCII_FORMAT                    0xc0
#define BINARY_FORMAT                   0x00
///@}

///@}

// RECORD TYPE 12h

/**
 * @name system_power_state_notification
 * @{
 */
#define SYS_PWR_STATE_NOTIF_REQUIRED    0X80
#define DEV_PWR_STATE_NOTIF_REQUIRED    0X40
#define DYNAMIC_CONTROLLER              0x00
#define STATIC_CONTROLLER               0x20
///@}

/**
 * @name controller_logs_initialization_errors
 * @{
 */
#define APPLICABLE_CONTROLLER_LOGS      0x08
///@}

/**
 * @name log_initialization_agent
 * @{
 */
#define AGENT_ACCESS_THIS_CONTROLLER    0x04
///@}

/**
 * @name event_message_generation
 * @{
 */
#define ENABLE_EVENT_MSG_GENERATION_FROM_CONTROLLER   0x00
#define DISABLE_EV_MSG_FROM_CONTROLLER  0x01
#define DO_NOT_INITIALIZE_CONTORLLER    0x02
///@}


/**
 * @name device_capabilities
 * @{
 */
#define CHASSIS_DEVICE_SUPPORTED        0x80
#define BRIDGE_NETFN_SUPPORTED          0x40
#define EVENT_GENERATOR_SUPPORTED       0x20
#define EVENT_RECEIVER_SUPPORTED        0x10
#define INVENTORY_DEVICE_SUPPORTED      0x08
#define SEL_DEVICE_SUPPORTED            0x04
#define SDR_REPOSITORY_DEVICE_SUPPORTED 0x02
#define SENSOR_DEVICE_SUPPORTED         0x01

/**
 * @name entity_instance (for SDR type 12h)
 * @{
 */
#define ENTITY_INSTANCE_01              0x01
#define ENTITY_INSTANCE_02              0x02
#define ENTITY_INSTANCE_03              0x03 // 04, 05, ...
///@}

///@}

/**
 * @{
 * @name Types of SDR
 */

/**
 * Enumeration of the SDR Types (definition in IPMI v1.5)
 *
 * IPMI v1.5 describes the content of each SDR Type. Based on this specification a structure was
 * designed in the OpenIPMC code for each type (currently only three types are implemented).
 */
typedef enum    {   EMPTY,
                    FULL_SENSOR_RECORD,                     // SDR TYPE 01h
                    COMPACT_SENSOR_RECORD,                  // SDR TYPE 02h
                    MANAGEMENT_CONTROLLER_DEVICE_LOCATOR    // SDR TYPE 12h
                }
                record_type_t;
///@}

/*
 * Structure to hold the reading of a sensor (1byte raw value) and return the threshold status of the sensor
 * 
 */
typedef struct{

    uint8_t  raw_value;
    uint16_t present_state;

}sensor_reading_t;


/**
 * This structure holds the content and type of all the SDRs created in the IPMC.
 * As the system is initialized, all the SDRs are created and stored in a {@link sensor_record_descriptor_t}
 * Global Array with size of {@link N_RECORDS_MAX}.
 *
 * The array starts with all the records with the {@link record_type_t} EMPTY.
 */
typedef struct
{
    record_type_t type;
    void*         ptr;
    void          (*get_sensor_reading_func)(sensor_reading_t*);
} sensor_record_descriptor_t;


/**
 * This structure contains all the fields in the Full Sensor Record (Record Type 01h, defined in IPMI v1.5).
 *
 * To create a sensor all the fields must be filled, in a way that Shelf Manager can interpret the information.
 */
typedef struct
{
    // Header
    uint16_t record_id;
    uint8_t sdr_version;
    uint8_t record_type_number;
    // Key Bytes
    uint8_t sensor_owner_id;
    uint8_t channel_number;
    uint8_t sensor_owner_lun;
    uint8_t sensor_number;
    // Body
    uint8_t entity_id;
    uint8_t entity_instance;
        // Initialization
    uint8_t initialization;
    uint8_t sensor_default_power_up_state;
        // Capabilities
    uint8_t sensor_status_check;
    uint8_t sensor_auto_rearm;
    uint8_t sensor_hysteresis_support;
    uint8_t sensor_threshold_access_support;
    uint8_t sensor_event_msg_ctrl_support;
    uint8_t sensor_type;
    uint8_t event_reading_type;
        // Masks
    uint16_t assertion_event_mask_for_non_threshold_sensor;
    uint8_t lower_threshold_reading_mask;
    uint16_t threshold_assertion_event_mask;
    uint16_t deassertion_event_mask_for_non_threshold_sensor;
    uint8_t upper_threshold_reading_mask;
    uint16_t threshold_deassertion_event_mask;
    uint16_t reading_mask_for_non_threshold_sensor;
    uint8_t settable_threshold_mask;
    uint8_t readable_threshold_mask;
        // Data Format
    uint8_t analog_data_format;
    uint8_t rate_unit;
    uint8_t modifier_unit;
    uint8_t percentage;
    uint8_t sensor_base_unit;
    uint8_t sensor_modifier_unit;
    uint8_t linearization;
    uint16_t m;
    uint8_t tolerance;
    uint16_t b;
    uint16_t accuracy;
    uint8_t accuracy_exp;
    uint8_t r_exp;
    uint8_t b_exp;
        // Ranges
    uint8_t analog_characteristics;
    uint8_t nominal_reading;
    uint8_t normal_maximum;
    uint8_t normal_minimum;
    uint8_t sensor_maximum_reading;
    uint8_t sensor_minimum_reading;
        // Threshold Settings
    uint8_t upper_non_recoverable_threshold;
    uint8_t upper_critical_threshold;
    uint8_t upper_non_critical_threshold;
    uint8_t lower_non_recoverable_threshold;
    uint8_t lower_critical_threshold;
    uint8_t lower_non_critical_threshold;
    uint8_t positive_threshold_hysteresis_value;
    uint8_t negative_threshold_hysteresis_value;
        // ID String Config
    uint8_t id_string_type;
    size_t id_string_length;
    char* id_string;
} sdr_type_01_t;

/**
 * This structure contains all the fields in the Compact Sensor Record (Record Type 02h, defined in IPMI v1.5).
 *
 * To create a sensor all the fields must be filled, in a way that Shelf Manager can interpret the information.
 */
typedef struct
{
    // Header
    uint16_t record_id;
    uint8_t sdr_version;
    uint8_t record_type_number;
    // Key Bytes
    uint8_t sensor_owner_id;
    uint8_t sensor_owner;
    uint8_t channel_number;
    uint8_t sensor_owner_lun;
    uint8_t sensor_number;
    // Body
    uint8_t entity_id;
    uint8_t entity_instance;
    uint8_t initialization;
    uint8_t sensor_default_power_up_state;
    uint8_t sensor_status_check;
    uint8_t sensor_auto_rearm;
    uint8_t sensor_hysteresis_support;
    uint8_t sensor_threshold_access_support;
    uint8_t sensor_event_msg_ctrl_support;
    uint8_t sensor_type;
    uint8_t event_reading_type;
    uint16_t assertion_event_mask_for_non_threshold_sensor;
    uint8_t lower_threshold_reading_mask;
    uint16_t threshold_assertion_event_mask;
    uint16_t deassertion_event_mask_for_non_threshold_sensor;
    uint8_t upper_threshold_reading_mask;
    uint16_t threshold_deassertion_event_mask;
    uint16_t reading_mask_for_non_threshold_sensor;
    uint8_t settable_threshold_mask;
    uint8_t readable_threshold_mask;
    uint8_t analog_data_format;
    uint8_t rate_unit;
    uint8_t modifier_unit;
    uint8_t percentage;
    uint8_t sensor_base_unit;
    uint8_t sensor_modifier_unit;
    uint8_t id_string_instance_modifier_type;
    uint8_t share_count;
    uint8_t entity_instance_sharing;
    uint8_t id_string_instance_modifier_offset;
    uint8_t positive_threshold_hysteresis_value;
    uint8_t negative_threshold_hysteresis_value;
    uint8_t id_string_type;
    size_t id_string_length;
    char* id_string;
} sdr_type_02_t;

/**
 * This structure contains all the fields in the Management Controller Device Locator Record (Record Type 12h, defined in IPMI v1.5).
 *
 * To create a sensor all the fields must be filled, in a way that Shelf Manager can interpret the information.
 */
typedef struct
{
    // Header
    uint16_t record_id;
    uint8_t sdr_version;
    uint8_t record_type_number;
    uint8_t record_length;
    // Key Bytes
    uint8_t device_slave_address;
    uint8_t channel_number;
    // Body
    uint8_t power_state_notification;
    uint8_t global_initialization;
    uint8_t device_capabilities;
    uint8_t entity_id;
    uint8_t entity_instance;
    uint8_t id_string_type;
    size_t id_string_length;
    char* id_string;
} sdr_type_12_t;


#endif /* SDR_DEFINITIONS_H */
