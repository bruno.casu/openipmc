/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file fru_inventory_manager.h
 * 
 * @author Bruno Augusto Casu
 * 
 * @brief  Management interface for the FRU Inventory creation and access.
 *
 * This header file has the definitions of the FRU Inventory managing functions and the struct used to transition from the individual fields of the
 * Inventory, created separately, to a single array that contains all the info for the FRU. 
 * 
 * Also, the file contains the definition of the
 * function to return the requested bytes from Shelf Manager in response to the "Read FRU Data" command.
 */

#ifndef SRC_OPENIPMC_SRC_FRU_INVENTORY_MANAGER_H_
#define SRC_OPENIPMC_SRC_FRU_INVENTORY_MANAGER_H_

#define ASCII_CODE 0xc0

/**
 * Global Array variable that contains the <b>FRU Inventory</b> bytes.
 *
 * The array is created using a sequence of created fields (defined in IPMI - Platform Management FRU Information Storage Definition v1.0)
 * and inserting each field array in this variable, which will be stored for further access.
 *
 * The size of this array is flexible and depends on the size of the fields that are created independently.
 *
 * @sa ipmi_nefn_storage.c
 */
extern uint8_t *fru_inventory;

/**
 * Size of the FRU inventory
 * 
 * @sa ipmi_cmd_get_fru_inventory_area_info().
 */
extern size_t fru_inventory_size;


/**
 * This struct is used to allocate the individual fields of the FRU Inventory. It holds the pointer of the data array 
 * and the area of the specific field (the area is in multiple of 8 bytes).
 * 
 * @note After the full Inventory is allocated in the definitive array, the pointer of the struct can be freed.
 * @sa create_fru_inventory()
 */
typedef struct
{
    size_t field_area;
    uint8_t *field_ptr;
} fru_inventory_field_t;

/**
 * @{
 * @name FRU Inventory Management Functions
 */

/**
 * @brief Specific function to create the Board Info field of the FRU Inventory.
 *
 * @param board_info Array that holds the data for the Board Info field.
 * @param mfg_date_time Manufacturing date and time (the value must be in minutes since 00:00h January 1th 1996).
 * @param board_manufacturer User defined string to identify the Board Manufacturer (max of 64 ASCII char).
 * @param board_product_name User defined string to identify the Product Name (max of 64 ASCII char).
 * @param board_serial_number User defined string to identify the Serial Number of the board (max of 64 ASCII char).
 * @param board_part_number User defined string to identify the Part Number of the board (max of 64 ASCII char).
 * @param fru_file_id Identification of the File.
 * 
 * @return The create function returns the area of the Board Info field (the value is multiple of 8 bytes).
 * 
 * This function is to create the Board Info field in the FRU Inventory. The function takes the string inputs from the user and stores the data in a
 * {@link fru_inventory_field_t} type struct, along with the area of the field. The create struct is then used in the full FRU Inventory create function.
 * 
 */
size_t create_board_info_field (uint8_t board_info[],
                                uint32_t mfg_date_time,
                                char * const board_manufacturer,
                                char * const board_product_name,
                                char * const board_serial_number,
                                char * const board_part_number,
                                char * const fru_file_id);

// Create function for the Multi Record Field (currently empty)
size_t create_multi_record_field (uint8_t multi_record[]);

/**
 * @brief Function to create the FRU Inventory.
 *
 * @param internal_use Pointer for the generated struct by the {@link create_board_info_field ()} function.  
 * @param chassis_info Pointer for the generated struct by the create filed function.
 * @param board_info   Pointer for the generated struct by the create filed function.
 * @param product_info Pointer for the generated struct by the create filed function.
 * @param multi_record Pointer for the generated struct by the create filed function.
 * 
 * Main function insert the Inventory fields created separately in the {@link fru_inventory} array, which can be accesses by the get_inventory_data() function
 * to return requested data from the inventory. FRU size, in bytes, is stored in the global {@link fru_inventory_size}.
 */
void create_fru_inventory ( fru_inventory_field_t *internal_use,
                            fru_inventory_field_t *chassis_info,
                            fru_inventory_field_t *board_info,
                            fru_inventory_field_t *product_info,
                            fru_inventory_field_t *multi_record);

/**
 * @brief Function to retrieve from FRU Inventory the requested bytes in the "Get FRU Data" IPMI command.
 * 
 * @param offset_in_record Offset byte in the Inventory to start the reading
 * @param bytes_to_read Number of bytes to be read from the Inventory
 * @param fru_id ID for the Field Replaceable Unit
 * @param record_data_buff Buffer to return the requested data
 * 
 * This function is called by the ipmi_cmd_read_fru_data() response function to retrieve the requested bytes from the {@link fru_inventory} array.
 * The bytes are loaded into a buffer and returned to the response function to be forward by the message manager and transmitted via IPMB.
 */
void  get_inventory_data (size_t offset_in_record, size_t bytes_to_read, uint8_t fru_id, uint8_t record_data_buff[]);

///@}

#endif /* SRC_OPENIPMC_SRC_FRU_INVENTORY_MANAGER_H_ */
