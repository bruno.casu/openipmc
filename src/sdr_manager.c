/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file sdr_manager.c
 *
 * @author Bruno Augusto Casu
 *
 * @brief  Management interface for SDR creation and conversion to binary format.
 */

#include "FreeRTOS.h"

#include "string.h"
#include "stdint.h"

#include "sdr_manager.h"

sensor_record_descriptor_t allrecords[N_RECORDS_MAX];

/**
 * @brief Function to convert the {@link sdr_type_01_t} struct into a binary format.
 *
 * @param sdr_ptr pointer of the SDR data in the {@link sdr_type_01_t} struct requested.
 * @param sdr_binary pointer of the SDR in binary format (array).
 *
 * This function converts the data in an {@link sdr_type_01_t } struct to a binary format (array).
 * The function is called by get_sdr_data(), as a request to read an SDR arrives, and the struct with the
 * SDR data needs to be translated in a format that can be sent through IPMB.
 */
static void sdr_type_01_to_binary(sdr_type_01_t* sdr_ptr, uint8_t* sdr_binary);

/**
 * @brief Function to convert the {@link sdr_type_02_t} struct into a binary format.
 *
 * @param sdr_ptr pointer of the SDR data in the {@link sdr_type_02_t} struct requested.
 * @param sdr_binary pointer of the SDR in binary format (array).
 *
 * This function converts the data in an {@link sdr_type_02_t } struct to a binary format (array).
 * The function is called by get_sdr_data(), as a request to read an SDR arrives, and the struct with the
 * SDR data needs to be translated in a format that can be sent through IPMB.
 */
static void sdr_type_02_to_binary(sdr_type_02_t* sdr_ptr, uint8_t* sdr_binary);

/**
 * @brief Function to convert the {@link sdr_type_12_t} struct into a binary format.
 *
 * @param sdr_ptr pointer of the SDR data in the {@link sdr_type_12_t} struct requested.
 * @param sdr_binary pointer of the SDR in binary format (array).
 *
 * This function converts the data in an {@link sdr_type_12_t } struct to a binary format (array).
 * The function is called by get_sdr_data(), as a request to read an SDR arrives, and the struct with the
 * SDR data needs to be translated in a format that can be sent through IPMB.
 */
static void sdr_type_12_to_binary(sdr_type_12_t* sdr_ptr, uint8_t* sdr_binary);

/*
 * Allocate SDR repository struct with empty values 
 */
void init_sdr_repository(void)
{
    for (size_t i = 0; i< N_RECORDS_MAX; ++i)
    {
        allrecords[i].type = EMPTY;
        allrecords[i].ptr  = NULL;
        allrecords[i].get_sensor_reading_func = NULL;
    }
}

/*
 * Allocate SDR as requested by create sensor function
 */
uint16_t allocate_sdr(record_type_t type)
{
    for (uint16_t record_id = 1; record_id< N_RECORDS_MAX; record_id++)
    {
        if(allrecords[record_id].type == EMPTY)
        {
            allrecords[record_id].type = type;
            if (type == FULL_SENSOR_RECORD )
            {
                allrecords[record_id].ptr = pvPortMalloc( sizeof(sdr_type_01_t) );
            }
            else if(type == COMPACT_SENSOR_RECORD)
            {
                allrecords[record_id].ptr = pvPortMalloc( sizeof(sdr_type_02_t) );
            }
            else if(type == MANAGEMENT_CONTROLLER_DEVICE_LOCATOR)
            {
                allrecords[record_id].ptr = pvPortMalloc( sizeof(sdr_type_12_t) );
            }
            return record_id;
        }
    }
    return SDR_SPACE_FULL;
}

/*
 * Convert a SDR into binary format to be transmitted via IPMI
 */
void get_sdr_data ( size_t offset_in_record, size_t bytes_to_read, uint16_t record_id, uint8_t record_data_buff[])
{
    int i;
    uint8_t sdr_binary[64];

    // Convert SDR into binary array
    switch (allrecords[record_id].type)
    {
        case FULL_SENSOR_RECORD:
            sdr_type_01_to_binary( (sdr_type_01_t*)allrecords[record_id].ptr, sdr_binary);
            break;

        case COMPACT_SENSOR_RECORD:
            sdr_type_02_to_binary( (sdr_type_02_t*)allrecords[record_id].ptr, sdr_binary);
            break;

        case MANAGEMENT_CONTROLLER_DEVICE_LOCATOR:
            sdr_type_12_to_binary( (sdr_type_12_t*)allrecords[record_id].ptr, sdr_binary);
            break;

        case EMPTY:
            for(i=0;i<64;i++)
            {
                sdr_binary[i]=0;
            }
            break;
    }

    //Load only the requested bytes into buffer
    for (i=0; i<bytes_to_read; i++) 
        record_data_buff[i] = sdr_binary[offset_in_record + i];
}

/*
 * Load "sdr_binary" buffer with data from requested sensor (sensor data record type 01h) 
 */
static void sdr_type_01_to_binary(sdr_type_01_t* sdr_ptr, uint8_t* sdr_binary)
{
    int k;
    sdr_binary[0]  = (uint8_t) ((sdr_ptr->record_id & 0x00FF) >> 0); // Low  byte
    sdr_binary[1]  = (uint8_t) ((sdr_ptr->record_id & 0xFF00) >> 8); // High byte
    sdr_binary[2]  = (sdr_ptr->sdr_version);
    sdr_binary[3]  = (sdr_ptr->record_type_number);
    sdr_binary[4]  = (SDR_TYPE_01_BODY_LENGTH + sdr_ptr->id_string_length);
    sdr_binary[5]  = (sdr_ptr->sensor_owner_id);
    sdr_binary[6]  = ((sdr_ptr->channel_number | sdr_ptr->sensor_owner_lun) & 0xf3);
    sdr_binary[7]  = (sdr_ptr->sensor_number);
    sdr_binary[8]  = (sdr_ptr->entity_id);
    sdr_binary[9]  = (sdr_ptr->entity_instance);
    sdr_binary[10] = ((sdr_ptr->initialization | sdr_ptr->sensor_default_power_up_state) & 0x7f);
    sdr_binary[11] = (sdr_ptr->sensor_status_check | sdr_ptr->sensor_auto_rearm | sdr_ptr->sensor_hysteresis_support | sdr_ptr->sensor_threshold_access_support | sdr_ptr->sensor_event_msg_ctrl_support);
    sdr_binary[12] = (sdr_ptr->sensor_type);
    sdr_binary[13] = (sdr_ptr->event_reading_type);
    sdr_binary[14] = (((uint8_t) ((sdr_ptr->assertion_event_mask_for_non_threshold_sensor & 0xFF00) >> 8) | sdr_ptr->lower_threshold_reading_mask | (uint8_t) ((sdr_ptr->threshold_assertion_event_mask & 0xFF00) >> 8)) & 0x7f);
    sdr_binary[15] = ((uint8_t) ((sdr_ptr->assertion_event_mask_for_non_threshold_sensor & 0x00FF) >> 0) | (uint8_t) ((sdr_ptr->threshold_assertion_event_mask & 0x00FF) >> 0));
    sdr_binary[16] = (((uint8_t) ((sdr_ptr->deassertion_event_mask_for_non_threshold_sensor & 0xFF00) >> 8) | sdr_ptr->upper_threshold_reading_mask | (uint8_t) ((sdr_ptr->threshold_deassertion_event_mask & 0xFF00) >> 8)) & 0x7f);
    sdr_binary[17] = ((uint8_t) ((sdr_ptr->deassertion_event_mask_for_non_threshold_sensor & 0x00FF) >> 0) | (uint8_t) ((sdr_ptr->threshold_deassertion_event_mask & 0x00FF) >> 0));
    sdr_binary[18] = (((uint8_t) ((sdr_ptr->reading_mask_for_non_threshold_sensor & 0xFF00) >> 8) | sdr_ptr->settable_threshold_mask) & 0x7f);
    sdr_binary[19] = ((uint8_t) ((sdr_ptr->reading_mask_for_non_threshold_sensor & 0x00FF) >> 0) | sdr_ptr->readable_threshold_mask);
    sdr_binary[20] = (sdr_ptr->analog_data_format | sdr_ptr->rate_unit | sdr_ptr->modifier_unit | sdr_ptr->percentage);
    sdr_binary[21] = (sdr_ptr->sensor_base_unit);
    sdr_binary[22] = (sdr_ptr->sensor_modifier_unit);
    sdr_binary[23] = (sdr_ptr->linearization);
    sdr_binary[24] = (uint8_t) ((sdr_ptr->m & 0x00FF) >> 0);
    sdr_binary[25] = (uint8_t) ((sdr_ptr->m & 0x0300) >> 2);
    sdr_binary[26] = (uint8_t) ((sdr_ptr->b & 0x00FF) >> 0);
    sdr_binary[27] = ( (uint8_t) ((sdr_ptr->b & 0x0300) >> 2) | (uint8_t) ((sdr_ptr->accuracy & 0x003f) >> 0) );
    sdr_binary[28] = ( (uint8_t) ((sdr_ptr->accuracy & 0x03c0) >> 2) | sdr_ptr->accuracy_exp);
    sdr_binary[29] = (sdr_ptr->r_exp | sdr_ptr->b_exp);
    sdr_binary[30] = ((sdr_ptr->analog_characteristics) & 0x07);
    sdr_binary[31] = (sdr_ptr->nominal_reading);
    sdr_binary[32] = (sdr_ptr->normal_maximum);
    sdr_binary[33] = (sdr_ptr->normal_minimum);
    sdr_binary[34] = (sdr_ptr->sensor_maximum_reading);
    sdr_binary[35] = (sdr_ptr->sensor_minimum_reading);
    sdr_binary[36] = (sdr_ptr->upper_non_recoverable_threshold);
    sdr_binary[37] = (sdr_ptr->upper_critical_threshold);
    sdr_binary[38] = (sdr_ptr->upper_non_critical_threshold);
    sdr_binary[39] = (sdr_ptr->lower_non_recoverable_threshold);
    sdr_binary[40] = (sdr_ptr->lower_critical_threshold);
    sdr_binary[41] = (sdr_ptr->lower_non_critical_threshold);
    sdr_binary[42] = (sdr_ptr->positive_threshold_hysteresis_value);
    sdr_binary[43] = (sdr_ptr->negative_threshold_hysteresis_value);
    sdr_binary[44] = (0); // RESERVED
    sdr_binary[45] = (0); // RESERVED
    sdr_binary[46] = (0); // RESERVED
    sdr_binary[47] = (sdr_ptr->id_string_type | sdr_ptr->id_string_length);
    for(k=0 ; k < sdr_ptr->id_string_length && k < 16 ; k++)
        sdr_binary[SDR_TYPE_01_FIXED_LENGTH+k]=sdr_ptr->id_string[k];
}

/*
 * Load "sdr_binary" buffer with data from requested sensor (sensor data record type 02h) 
 */
static void sdr_type_02_to_binary(sdr_type_02_t* sdr_ptr, uint8_t* sdr_binary)
{
    int k;
    sdr_binary[0]  = (uint8_t) ((sdr_ptr->record_id & 0x00FF) >> 0); // Low  byte
    sdr_binary[1]  = (uint8_t) ((sdr_ptr->record_id & 0xFF00) >> 8); // High byte
    sdr_binary[2]  = (sdr_ptr->sdr_version);
    sdr_binary[3]  = (sdr_ptr->record_type_number);
    sdr_binary[4]  = (SDR_TYPE_02_BODY_LENGTH + sdr_ptr->id_string_length);
    sdr_binary[5]  = (sdr_ptr->sensor_owner_id);
    sdr_binary[6]  = ((sdr_ptr->channel_number | sdr_ptr->sensor_owner_lun) & 0xf3);
    sdr_binary[7]  = (sdr_ptr->sensor_number);
    sdr_binary[8]  = (sdr_ptr->entity_id);
    sdr_binary[9]  = (sdr_ptr->entity_instance);
    sdr_binary[10] = ((sdr_ptr->initialization | sdr_ptr->sensor_default_power_up_state) & 0x7f);
    sdr_binary[11] = (sdr_ptr->sensor_status_check | sdr_ptr->sensor_auto_rearm | sdr_ptr->sensor_hysteresis_support | sdr_ptr->sensor_threshold_access_support | sdr_ptr->sensor_event_msg_ctrl_support);
    sdr_binary[12] = (sdr_ptr->sensor_type);
    sdr_binary[13] = (sdr_ptr->event_reading_type);
    sdr_binary[14] = (((uint8_t) ((sdr_ptr->assertion_event_mask_for_non_threshold_sensor & 0xFF00) >> 8) | sdr_ptr->lower_threshold_reading_mask | (uint8_t) ((sdr_ptr->threshold_assertion_event_mask & 0xFF00) >> 8)) & 0x7f);
    sdr_binary[15] = ((uint8_t) ((sdr_ptr->assertion_event_mask_for_non_threshold_sensor & 0x00FF) >> 0) | (uint8_t) ((sdr_ptr->threshold_assertion_event_mask & 0x00FF) >> 0));
    sdr_binary[16] = (((uint8_t) ((sdr_ptr->deassertion_event_mask_for_non_threshold_sensor & 0xFF00) >> 8) | sdr_ptr->upper_threshold_reading_mask | (uint8_t) ((sdr_ptr->threshold_deassertion_event_mask & 0xFF00) >> 8)) & 0x7f);
    sdr_binary[17] = ((uint8_t) ((sdr_ptr->deassertion_event_mask_for_non_threshold_sensor & 0x00FF) >> 0) | (uint8_t) ((sdr_ptr->threshold_deassertion_event_mask & 0x00FF) >> 0));
    sdr_binary[18] = (((uint8_t) ((sdr_ptr->reading_mask_for_non_threshold_sensor & 0xFF00) >> 8) | sdr_ptr->settable_threshold_mask) & 0x7f);
    sdr_binary[19] = ((uint8_t) ((sdr_ptr->reading_mask_for_non_threshold_sensor & 0x00FF) >> 0) | sdr_ptr->readable_threshold_mask);
    sdr_binary[20] = (sdr_ptr->analog_data_format | sdr_ptr->rate_unit | sdr_ptr->modifier_unit | sdr_ptr->percentage);
    sdr_binary[21] = (sdr_ptr->sensor_base_unit);
    sdr_binary[22] = (sdr_ptr->sensor_modifier_unit);
    sdr_binary[23] = ((sdr_ptr->id_string_instance_modifier_type | sdr_ptr-> share_count) &0x3f);
    sdr_binary[24] = (sdr_ptr-> entity_instance_sharing | sdr_ptr->id_string_instance_modifier_offset);
    sdr_binary[25] = (sdr_ptr->positive_threshold_hysteresis_value);
    sdr_binary[26] = (sdr_ptr->negative_threshold_hysteresis_value);
    sdr_binary[27] = (0); // RESERVED
    sdr_binary[28] = (0); // RESERVED
    sdr_binary[29] = (0); // RESERVED
    sdr_binary[30] = (0); // RESERVED
    sdr_binary[31] = (sdr_ptr->id_string_type | sdr_ptr->id_string_length);
    for(k=0 ; k < sdr_ptr->id_string_length && k < 16 ; k++)
        sdr_binary[SDR_TYPE_02_FIXED_LENGTH+k]=sdr_ptr->id_string[k];
}

/*
 * Load "sdr_binary" buffer with data from requested sensor (sensor data record type 12h) 
 */
static void sdr_type_12_to_binary(sdr_type_12_t* sdr_ptr, uint8_t* sdr_binary)
{
    int k;
    sdr_binary[0]  = (uint8_t) ((sdr_ptr->record_id & 0x00FF) >> 0); // Low  byte
    sdr_binary[1]  = (uint8_t) ((sdr_ptr->record_id & 0xFF00) >> 8); // High byte
    sdr_binary[2]  = (sdr_ptr->sdr_version);
    sdr_binary[3]  = (sdr_ptr->record_type_number);
    sdr_binary[4]  = (SDR_TYPE_12_BODY_LENGTH + sdr_ptr->id_string_length);
    sdr_binary[5]  = (sdr_ptr->device_slave_address);
    sdr_binary[6]  = (sdr_ptr->channel_number & 0x0f);
    sdr_binary[7]  = ((sdr_ptr->power_state_notification | sdr_ptr->global_initialization) & 0xef);
    sdr_binary[8]  = (sdr_ptr->device_capabilities);
    sdr_binary[9]  = (0); // RESERVED
    sdr_binary[10] = (0); // RESERVED
    sdr_binary[11] = (0); // RESERVED
    sdr_binary[12] = (sdr_ptr->entity_id);
    sdr_binary[13] = (sdr_ptr->entity_instance);
    sdr_binary[14] = (0); // RESERVED
    sdr_binary[15] = (sdr_ptr->id_string_type | sdr_ptr->id_string_length);
    for(k=0 ; k < sdr_ptr->id_string_length && k < 16 ; k++)
        sdr_binary[SDR_TYPE_12_FIXED_LENGTH+k]=sdr_ptr->id_string[k];
}
