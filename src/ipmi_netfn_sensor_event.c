/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file ipmi_netfn_sensor_event.c
 *
 * @authors Alison Franca Queiroz da Costa
 * @authors Andre Muller Cascadan
 * @authors Bruno Augusto Casu
 *
 * @brief  Response functions for Sensor Event (S/E) Network Function commands.
 *
 * This file contains the specific functions to manage the requests of Sensor and Event Network Function commands (definition in PICMG v3.0 and IPMI v1.5).
 * Each function returns the response data bytes and the completion code.
 *
 * The specific functions are called by ipmi_msg_solve_request_ipmb0().
 */

#include "string.h"
#include "stdint.h"

#include "sdr_manager.h"
#include "ipmc_ios.h"

/**
 * @{
 * @name Sensor Event NetFn commands
 */

/**
 * @brief Specific function to provide a response for "Set Event Receiver" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_set_event_rec( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    ipmc_ios_printf("Set Event Receiver\r\n");

    if(request_bytes[0] == 0x20 && request_bytes[1] == 0x00) // Check IPMB Slave Address and Event Receiver LUN
    {
        *res_len = 0;
        *completion_code = 0; // OK
    }

    else
    {
        *res_len = 0;
        *completion_code = 0xCC; // Invalid data field in Request
    }
}

/**
 * @brief Specific function to provide a response for "Reserve Device SDR Repository" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_reserve_device_sdr_repos( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    ipmc_ios_printf("Reserve Device SDR Repository\r\n");

    response_bytes[0] = RESERVATION_ID_LSB; // Reservation ID (Least Significant Byte)
    response_bytes[1] = RESERVATION_ID_MSB; // Reservation ID (Most Significant Byte)
    *res_len = 2;
    *completion_code = 0; // OK
}

/**
 * @brief Specific function to provide a response for "Get Device SDR" command.definition
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 * 
 * This function is part of the SDR management. As a Get Device SDR command is received this function calls get_sdr_data()
 * to access SDR repository and retrieve the requested data.
 * 
 * @sa get_sdr_data()
 */
void ipmi_cmd_get_device_sdr( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    int i;
    ipmc_ios_printf("Get Device SDR\r\n");

    if (request_bytes[0] == RESERVATION_ID_LSB && request_bytes[1] == RESERVATION_ID_MSB) // Check reserve ID
    {
        uint8_t record_data_buff[64]; // Maximum size of a SDR is 64 bytes (definition in IPMI v1.5)

        size_t             bytes_to_read    = request_bytes[5];
        size_t       const offset_in_record = request_bytes[4];
        uint8_t      const record_id_ms     = request_bytes[3];
        uint8_t      const record_id_ls     = request_bytes[2];
        uint16_t     const record_id        = (( (uint16_t)record_id_ms ) << 8) + (( (uint16_t)record_id_ls ) << 0);

        get_sdr_data (offset_in_record, bytes_to_read, record_id, record_data_buff); // Access repository and load record data to buffer

        // Inform next record ID
        uint16_t record_id_next;
        uint16_t aux = 1;
        while (allrecords[aux].type != EMPTY) // Search for last record id
            {
                aux++; // number of the last record = number of sensors
            }

        if (record_id == aux-1) record_id_next = 0xffff; // inform ShM this is the last record
        else record_id_next = record_id +1 ;

        response_bytes[0] = (uint8_t) ((record_id_next & 0x00FF) >> 0); // Low  byte
        response_bytes[1] = (uint8_t) ((record_id_next & 0xFF00) >> 8); // High byte

        if (bytes_to_read > 22) bytes_to_read = 22;      // Prevents from sending more bytes than IPMB support

        for (i = 0; i < bytes_to_read; i++)              
        {
            response_bytes[i+2] = record_data_buff[i]; // Load response bytes from buffer
        }

        *res_len = bytes_to_read +2;
        *completion_code = 0; // OK
    }

    else
    {
        *res_len = 0;
        *completion_code = 0x80;  // Reserve ID incorrect
    }
}

/**
 * @brief Specific function to provide a response for "Get Device SDR Info" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_get_device_sdr_info( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    ipmc_ios_printf("Get Device SDR Info\r\n");

    uint16_t n = 0;
    
    for (int aux=0 ; aux<=N_RECORDS_MAX ; aux++)
    {
    	if (allrecords[aux].type != EMPTY)
    	{
    		n++;
    	}
    }

    /*
    while (allrecords[aux].type != EMPTY) // Search for last record id
    {
        aux++; // number of the last record = number of sensors
    }
    */

    response_bytes[0] = n; // Number of Sensors in Device (counting the Management Controller)
    
    response_bytes[1] = 0x01; // [7]   0 = Static sensor population
                              // [6:4] Reserved  
                              // [3]   1 = LUN 3 has sensors
                              // [2]   1 = LUN 2 has sensors
                              // [1]   1 = LUN 1 has sensors
                              // [0]   1 = LUN 0 has sensors

    *res_len = 2;
    *completion_code = 0; // OK
}

/**
 * @brief Specific function to provide a response for "Get Sensor Reading" command.
 *
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_get_sensor_reading( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{

    uint8_t sensor_number;
    sensor_reading_t reading;
    
    ipmc_ios_printf("Get Sensor Reading\r\n");
    
    sensor_number = request_bytes[0];
    
    if (allrecords[sensor_number].get_sensor_reading_func != NULL) // Assume Sensor Number and RecordID are the same (for this implementation)
    {
      (*(allrecords[sensor_number].get_sensor_reading_func))(&reading);
      
      response_bytes[0] = reading.raw_value; // Temperature °C (Linear, Unsigned value)
      response_bytes[1] = 0xC0; // Event messages and Sensor scanning are enabled
      response_bytes[2] = (uint8_t) (reading.present_state & 0x00FF);     // Present State flags
      response_bytes[3] = (uint8_t)((reading.present_state & 0xFF00)>>8);
      *res_len = 4;
      *completion_code = 0;     // OK
    }
    else
    {
      *res_len = 0;
      *completion_code = 0xCB;     // Sensor does not exist
    }

}

/**
 * @brief Specific function to provide a response for "Set Sensor Event Enable" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_set_sensor_event_enable( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    ipmc_ios_printf("Set Sensor Event Enable\r\n");

    *res_len = 0;
    *completion_code = 0; // OK
}

/**
 * @brief Specific function to provide a response for "Get Sensor Event Status" command.
 *
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_get_sensor_event_status( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    ipmc_ios_printf("Get Sensor Event Status\r\n");

    switch(request_bytes[0])
    {
        case 0x03: // DUMMY SENSOR FPGA TEMPERATURE
            response_bytes[0] = 0xC0; // Event msg and Sensor scanning are enable
            response_bytes[1] = 0x00; // Assertion event for non critical and low critical thresholds (threshold crossing on upper or lower condition)
            response_bytes[2] = 0x00; // Assertion event for critical and non recov. thresholds (threshold crossing on upper or lower condition)
            response_bytes[3] = 0x00; // De-assertion for non critical and low critical thresholds
            response_bytes[4] = 0x00; // De-assertion for critical and non recov. thresholds
            *res_len = 5;
            *completion_code = 0; // OK
        break;

        case 0x04: // DUMMY SENSOR AIR TEMPERATURE
            response_bytes[0] = 0xC0; // Event msg and Sensor scanning are enable
            response_bytes[1] = 0x00; // Assertion event for non critical and low critical thresholds (threshold crossing on upper or lower condition)
            response_bytes[2] = 0x00; // Assertion event for critical and non recov. thresholds (threshold crossing on upper or lower condition)
            response_bytes[3] = 0x00; // De-assertion for non critical and low critical thresholds
            response_bytes[4] = 0x00; // De-assertion for critical and non recov. thresholds
            *res_len = 5;
            *completion_code = 0; // OK
        break;

        default:
        *res_len = 0;
        *completion_code = 0xCB; // Sensor does not exist
    }
}

///@}
