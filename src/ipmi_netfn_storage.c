/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file ipmi_netfn_storage.c
 * 
 * @authors Bruno Augusto Casu
 * 
 * @brief  Response functions for Storage Network Function commands.
 *
 * This file contains the specific functions to manage the requests of Storage Network Function commands (definition in PICMG v3.0 and IPMI v1.5).
 * Each function returns the response data bytes and the completion code.
 * 
 * The specific functions are called by ipmi_msg_solve_request_ipmb0().    
 */

#include "string.h"
#include "stdint.h"

#include "fru_inventory_manager.h"
#include "ipmc_ios.h"

/**
 * @{
 * @name Storage NetFn commands
 */

/**
 * @brief Specific function to provide a response for "Get FRU Area Info" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_get_fru_inventory_area_info (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    ipmc_ios_printf("Get FRU Inventory Area Info\r\n");

    if (request_bytes[0] == 0x00) // Check if FRU ID is 00h
    {
        uint16_t const fru_inventory_area = fru_inventory_size;

        response_bytes[0] = (uint8_t) ((fru_inventory_area & 0x00ff) >> 0); // size of FRU Inventory data ls byte
        response_bytes[1] = (uint8_t) ((fru_inventory_area & 0xff00) >> 8); // size of FRU Inventory data ms byte
        response_bytes[2] = 0x00; // 00h = device is accessed by bytes

        *res_len = 3;
        *completion_code = 0; // OK
    }

    else
    {
        *res_len = 0;
        *completion_code = 0xCC;  // Invalid data field in request
    }
}

/**
 * @brief Specific function to provide a response for "Read FRU Data" command.
 *
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 *
 * This function is part of the Inventory management. As a Read FRU Data command is received this function calls get_inventory_data()
 * to access inventory and retrieve the requested data.
 */
void ipmi_cmd_read_fru_data( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    int i;
    ipmc_ios_printf("Read FRU Data\r\n");

    uint8_t record_data_buff[16];

    uint8_t      const fru_id    = request_bytes[0]; // FRU Device ID
    uint8_t      const offset_ms = request_bytes[2];
    uint8_t      const offset_ls = request_bytes[1];
    uint16_t     const offset = (( (uint16_t)offset_ms ) << 8) + (( (uint16_t)offset_ls ) << 0);

    size_t  const offset_in_record = offset;
    size_t  const bytes_to_read = request_bytes[3];

    get_inventory_data (offset_in_record, bytes_to_read, fru_id, record_data_buff); // Access inventory and load FRU data to buffer

    for (i = 0; i < bytes_to_read; i++)
    {
        response_bytes[i+1] = record_data_buff[i]; // Load response bytes from buffer
    }

    response_bytes[0] = request_bytes[3];
    *res_len = bytes_to_read +1;
    *completion_code = 0; // OK
}

///@}
