/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file sdr_manager.h
 *
 * @author Bruno Augusto Casu
 *
 * @brief  Management interface for SDR creation and conversion to binary format.
 *
 * This header file has the definition of the functions that allocates the SDRs in the struct format with user and predefined parameters.
 * Also the functions to reorganize the SDR from the struct to a binary form, which can be read and transmited by the IPMI command response function.
 */

#include "sdr_definitions.h"

#ifndef SDR_MANAGER_H
#define SDR_MANAGER_H

/**
 * @{
 * @name Reservation ID
 */

/**
 * Most Significant Byte of the Reservation ID. 
 * @sa ipmi_cmd_reserve_device_sdr_repos()
 */
#define RESERVATION_ID_MSB 0x99
/**
 * Least Significant Byte of the Reservation ID. 
 * @sa ipmi_cmd_reserve_device_sdr_repos()
 */
#define RESERVATION_ID_LSB 0x99
///@}

/**
 * Value returned by the allocate_sdr() function to inform that there are no more empty records available.
 */
#define SDR_SPACE_FULL     0xffff

/**
 * Value used to set maximum number of records in the {@link allrecords} global array.
 */
#define N_RECORDS_MAX 128

/**
 * @{
 * @name Number of following bytes after header in SDR (without the id string)
 */

/*
 * These macros are used in the SDR struct conversion to binary functions.
 * They indicate the number of remaining bytes after the header, in the SDR (definition in IPMI v1.5).
 * 
 * This value does not take into account the size of the id string.
 */
#define SDR_TYPE_01_BODY_LENGTH 0x2B 
#define SDR_TYPE_02_BODY_LENGTH 0x1B 
#define SDR_TYPE_12_BODY_LENGTH 0x0B 
///@}

/**
 * @{
 * @name Number or bytes in the SDR without (the id string)
 */

/*
 * These macros are used in the SDR struct conversion to binary functions.
 * At the end of the binary conversion this fixed size is used to set the offset for the id string.
 * 
 * This value take into account the header size, but not the id string size.
 */
#define SDR_TYPE_01_FIXED_LENGTH 0x30 //maximum length is 64bytes: fixed size is 48bytes with header (5bytes) and id string has maximum of 16bytes
#define SDR_TYPE_02_FIXED_LENGTH 0x20 //maximum length is 48bytes: fixed size is 32bytes with header (5bytes) and id string has maximum of 16bytes
#define SDR_TYPE_12_FIXED_LENGTH 0x10 //maximum length is 32bytes: fixed size is 16bytes with header (5bytes) and id string has maximum of 16bytes
///@}

/**
 * Global Array variable that contains all the SDRs (IPMI v1.5 defines this as the <b>Sensor Data Record (SDR) Repository</b>).
 *
 * Each position of the array holds the type of the SDR ({@link record_type_t}) and the pointer of an specific struct ({@link sdr_type_01_t}, {@link sdr_type_02_t} OR {@link sdr_type_12_t}),
 * corresponding with the SDR type.
 *
 * The size of this array is defined by the {@link N_RECORDS_MAX} macro. The initial design of OpenIPMC code holds a maximum of 128 Records in the repository
 *
 * @note The Record ID of an SDR is defined by the position of the record in this array
 *       (example: if a create function allocates allrecords[3], the SDR created will have Record ID 0003h.
 *
 * @sa init_sdr_repository().
 */
extern sensor_record_descriptor_t allrecords[N_RECORDS_MAX];

/**
 * @{
 * @name SDR Management Functions
 */

/**
 * @brief Initialize the Global Array {@link allrecords}.
 * 
 * This function starts the SDR creation by allocating the Global Array {@link allrecords} where all SDR types and pointers will be stored.
 * 
 * When Shelf Manager sends a Get Device SDR command, the IPMC locates the record in the {@link allrecords} struct by the Record ID.
 */
void init_sdr_repository(void);

/**
 * @brief This function allocates an empty slot of {@link allrecords} for a new SDR creation.
 *
 * @param type Type of the SDR that will be allocated in the Array.
 *
 * @return The function returns the allocated Record ID, based on the position of the record in the array.
 *
 * This function is called in the beginning of all SDR creation functions, with the desired type of SDR to be created.
 * It will search in the {@link allrecords} Array for an {@link record_type_t} EMPTY record.
 *
 * @note Is important to mention that this function returns the Record ID sequentially. This means that the order of creation of SDRs
 *       set the Record ID for each record.
 *
 * @sa create_management_controller_sdr() for an example of usage of this function.
 */
uint16_t allocate_sdr(record_type_t type);

/**
 * @brief Function to locate an SDR struct in the records and return the requested data bytes from the record.
 * 
 * @param offset_in_record Offset byte in the IPMI request message.
 * @param bytes_to_read Number of bytes to be read from the Record.
 * @param record_id Reference to locate the record in the repository.
 * @param record_data_buff Buffer to return the requested bytes.
 * 
 * This function is called by the response function ipmi_cmd_get_device_sdr() with the parameters of the Shelf Manger IPMI request (Get Device SDR command).
 * The function will then locate the desired record using the Record ID. As the record is located a conversion to binary function is called
 * to convert the SDR strcut into a binary format, that will be loaded into the buffer. The function then returns the buffer pointer to the
 * response function.
 * 
 * @note in the case of a request of an {@link record_type_t} EMPTY record, the buffer will be loaded with zeros.
 * 
 * @sa sdr_type_01_to_binary(), sdr_type_02_to_binary(), sdr_type_12_to_binary(). 
 */
void get_sdr_data ( size_t offset_in_record, size_t bytes_to_read, uint16_t record_id, uint8_t record_data_buff[]);


///@}

#endif // SDR_MANAGER_H

