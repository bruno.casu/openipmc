# OpenIPMC

**OpenIPMC** is a free open source Intelligent Platform Management Controller (IPMC) for AdvancedTCA&reg; boards following the PICMG 3.0 standard.

OpenIPMC is written in C as a platform-independent source, designed to run ontop of the __[FreeRTOS](www.freertos.org)__ real-time operating system, such that it can be easily ported the many embedded microcontroller architectures that support FreeRTOS. In the OpenIPMC source code, callbacks and other helper functions allow to flexibly customize the implementation of the IOs and __payload__ interfaces, easing the porting to new hardware architectures.

## Documentation

This project and its documentation are under development. Doxygen pages can be found __[here](https://openipmc.gitlab.io/openipmc)__

## Authors and Affiliates

- Alison Franca Queiroz da Costa (UNESP)
- André Muller Cascadan (UNESP)
- Bruno Casu (FEI)
- Lucas Arruda Ramalho (UNEMAT)
- Luigi Calligaris (UNESP)

This project started in 2019 at the Center for Scientific Computing of the São Paulo State University (NCC-UNESP, Brazil) as a contribution from __[SPRACE](https://sprace.org.br/)__ to the CMS Tracker Phase2 Upgrade (CERN). 

## Contributors

- Luis Ardila Perez (KIT)
- Oliver Sander (KIT)

## Acknowledgment

We acknowledge the Karlsruhe Institute of Technology - KIT for supporting our project by providing equipment, development kits, and a complete ATCA environment for testing this project on the Serenity ATCA board (Imperial College).

## Related Publications

- __[T .C. Paiva (2016). Remote development environment with reconfigurable components in the Advanced Telecom Computing Architecture context. (Master thesis) São Paulo State University, Brazil](http://hdl.handle.net/11449/144448)__

- __[L. A. Ramalho et al., "Development of an Intelligent Platform Management controller for the Pulsar IIb," 2015 IEEE Nuclear Science Symposium and Medical Imaging Conference (NSS/MIC), San Diego, CA, 2015, pp. 1-2.](http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=7581788&isnumber=7581238)__

- __[M. Vaz, A.M. Cascadan, V.F. Ferreira, T. Paiva, L.A. Ramalho, A.A. Shinoda. “A Framework for Development and Test of xTCA Modules With FPGA Based Systems for Particle Detectors”, International Workshop on Personal Computers and Particle Accelerator Controls (11th), Campinas, Brazil, 2016](https://doi.org/10.18429/JACoW-PCaPAC2016-THDAPLCO06)__
