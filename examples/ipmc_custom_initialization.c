/******************************************************************************

MIT License

Copyright (c) 2019 Bruno Casu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

******************************************************************************/



/*
 * ipmc_custom_initialization.c
 *
 *  Created on: 2 de out de 2019
 *      Author: brunocasu
 */


/* FreeRTOS includes. */
#include "FreeRTOS.h"

//OpenIPMC includes
#include "../src/sdr_manager.h"
#include "../src/power_manager.h"
#include "../src/fru_inventory_manager.h"
#include "../src/sdr_definitions.h"
#include "../src/ipmc_ios.h"
#include "../src/sensors_templates.h"

void power_initialization(void);

uint8_t test_temp_value = 50;

void sensor_reading_fpga_temp(sensor_reading_t* sensor_reading);
void sensor_reading_air_temp(sensor_reading_t* sensor_reading);
void sensor_reading_vcc_out(sensor_reading_t* sensor_reading);

void ipmc_custom_initialization()
{

	/* 
	 * Power Initialization.
	 * 
	 * To simplify the example, the Power Initialization was move to to another file.
	 * See ipmc_power.c file
	 */
	power_initialization();

	/*
	 *  Create FRU Inventory
	 */
	fru_inventory_field_t *board_info   = pvPortMalloc(sizeof(fru_inventory_field_t)); // Inventory Fields must be allocated and filled before FRU Inventory is created
	board_info->field_ptr = pvPortMalloc(sizeof(uint8_t)*256);

	board_info->field_area = create_board_info_field (board_info->field_ptr,// MAXIMUM SIZE OF EACH STRING IS 64 CHARACTERS / FIELD AREA IS MULTIPLE OF 8 BYTES
														13017600, 			// Manufacturing Date/Time (in minutes from January 1th 1996)
														"SPRACE-KIT", 		// Board Manufacturer,
														"OpenIPMC-HW",		// Board Product Name,
														"000000-0001",	    // Serial Number,
														"XXXXXXX1",			// Part Number,
														"01");// FRU File ID

	create_fru_inventory (	NULL, // Internal Use Field NOT USED
   							NULL, // Chassis Info Field NOT USED
							board_info,
							NULL, // Product Info Field NOT USED
							NULL);// Multi Record Field NOT USED

	vPortFree (board_info->field_ptr);
	vPortFree (board_info);


	/*
	 *  Create Sensors
	 */
    init_sdr_repository();

    create_management_controller_sdr ("IPMC Zync US+");

    create_hot_swap_carrier_sensor ("Hot Swap Carrier");

    // PRARAMETERS for create_simple_temperature_sensor function:
    // base_unit (°C, °F...), 
    // linearization (linear, log10...),
    // m (Y = mX + b),
    // b,
    // accuracy,
    // upper non recoverable threshold, INSERT RAW DATA
    // upper critical threshold,        INSERT RAW DATA
    // upper non critical threshold,    INSERT RAW DATA
    // id string
    create_simple_temperature_sensor (DEGREES_C, LINEAR, 1, 0, 1, 85, 75, 65, "FPGA TEMP", &sensor_reading_fpga_temp);
    create_simple_temperature_sensor (DEGREES_C, LINEAR, 1, 0, 1, 65, 60, 55, "AIR TEMP", &sensor_reading_air_temp);

    // PRARAMETERS for create_voltage_out_sensor function:
    // linearization (linear, log10...),
    // m (Y = mX + b),
    // b,
    // accuracy,
    // r_exp, R = Y*10^r_exp (result exponent - SIGNED 4 bits, MSB of field); C0h = -4
    // upper non critical threshold, //  INSERT RAW DATA (must be converted using Y = mX +b and r_exp)
    // lower non critical threshold, //  INSERT RAW DATA (must be converted using Y = mX +b and r_exp)
    // id string
    create_simple_voltage_out_sensor(LINEAR, 157, 0, 1, 0xc0, 0x46, 0x39, "VCC1V0 VOUT", &sensor_reading_vcc_out);


}

void sensor_reading_fpga_temp(sensor_reading_t* sensor_reading)
{
	
	sensor_reading->raw_value = test_temp_value;
	
	if((test_temp_value >= 65) && (test_temp_value < 75))
		sensor_reading->present_state = (1<<3);
	else if(test_temp_value >= 75)
		sensor_reading->present_state = (1<<4);
	else
		sensor_reading->present_state = 0;
}


void sensor_reading_air_temp(sensor_reading_t* sensor_reading)
{
	
	sensor_reading->raw_value = 32;
	
	sensor_reading->present_state = 0;
	
}

void sensor_reading_vcc_out(sensor_reading_t* sensor_reading)
{
	sensor_reading->raw_value = 22;

	sensor_reading->present_state = 0;
}

void payload_cold_reset (void)
{
    // user defined for reseting the payload
    ipmc_ios_printf("\nCOLD RESET SUCCESSFUL!\r\n");

}

uint8_t get_fru_control_capabilities (void)
{
    uint8_t const capabilities = 0; // WARM_RESET_SUPPORTED | GRACEFUL_REBOOT_SUPPORTED | DIAGNOSTIC_INTERRUPT_SUPPORTED
    return capabilities;
}


