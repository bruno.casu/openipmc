/******************************************************************************

MIT License

Copyright (c) 2019 Bruno Casu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

******************************************************************************/



/*
 * ipmc_power.c
 *
 *  Created on: 19/nov/2019
 *      Author: cascadan 
 *              calligar
 */

#include <stdint.h>

//OpenIPMC includes
#include "../src/power_manager.h"
#include "../src/ipmc_ios.h"

void power_initialization(void)
{
	
	power_envelope_t pwr_envelope;
	
	pwr_envelope.num_of_levels =  2; // Max of 20 beyond the 0-th value
	pwr_envelope.multiplier    = 10; // Global multiplier in tenths of Watt
	
	// REMINDER: PICMG spec **requires** the maximum power envelope values in this array to increase monotonically!
	pwr_envelope.power_draw[ 0] = 0;   // Power Level 0: RESERVED and always means 0 Watt (payload off)
	pwr_envelope.power_draw[ 1] = 20;  // Power Level 1: 20 Watts ( power_draw[1] * multiplier * 0.1W = 20 * 10 * 0.1 W = 20W )
	pwr_envelope.power_draw[ 2] = 100; // Power Level 2: 100 Watts
	//pwr_envelope.power_draw[ 3] = 110;
	//pwr_envelope.power_draw[ 4] = 140;
	//pwr_envelope.power_draw[ 5] = 150;
	//pwr_envelope.power_draw[ 6] = 160;
	//pwr_envelope.power_draw[ 7] = 170;
	//pwr_envelope.power_draw[ 8] = 180;
	//pwr_envelope.power_draw[ 9] = 190;
	//pwr_envelope.power_draw[10] = 210;
	//pwr_envelope.power_draw[11] = 211;
	//pwr_envelope.power_draw[12] = 212;
	//pwr_envelope.power_draw[13] = 213;
	//pwr_envelope.power_draw[14] = 214;
	//pwr_envelope.power_draw[15] = 215;
	//pwr_envelope.power_draw[16] = 216;
	//pwr_envelope.power_draw[17] = 217;
	//pwr_envelope.power_draw[18] = 218;
	//pwr_envelope.power_draw[19] = 219;
	//pwr_envelope.power_draw[20] = 310;
	
	ipmc_pwr_setup_power_envelope(pwr_envelope);  // Copy the envelope to the power manager
	
	// Here must be informed the Power Level desired by the payload. It must be a valid index of the power_draw array (1 up to num_of_levels).
	ipmc_pwr_set_desired_power_level(2); // Power Level 2 means 100 Watts, following what is specified in the power_draw array above.
}



void ipmc_pwr_switch_power_level_on_payload(uint8_t new_power_level)
{
	
	ipmc_ios_printf("Change Power Level from %d to %d\r\n", ipc_pwr_get_current_power_level(), new_power_level);
	/*
	 * Do whatever is needed to set to the requested power level on the payload
	 */
	
	/*
	 * TODO: improve example with 12V_enable and reading of current power level
	 */
	
	return;
}





